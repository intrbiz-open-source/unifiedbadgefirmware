/*
 * LED driver prototypes
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef DRIVER_H
#define DRIVER_H

#include "program_stack.h"
#include "variant.h"

/* PWM Levels */
#define PWM_OFF  0
#define PWM_TOP 254
#define PWM_ON  255

/* Period unit (1/20th second) */
#define PERIOD_RESOLUTION 50

uint8_t scaleBrightness(uint8_t brightness);

uint8_t nextRandom(uint8_t min, uint8_t max);

void sleep_wait(uint8_t seconds);

void wait(uint8_t period);

void writeLine(uint8_t line, uint8_t value);

void refreshLines();

void fadeLine(uint8_t line, uint8_t value, uint8_t period);

void flashLed(uint8_t col, uint8_t row, uint8_t value, uint8_t period);

void flashLed(uint8_t led, uint8_t value, uint8_t period);

void builtInLedAllOff();

void builtInLedAllOn(uint8_t value, uint8_t period);

void builtInLedFlashRandom(uint8_t value, uint8_t period);

void builtInLedCustom(uint8_t target, uint8_t value, uint8_t period);

void setupDriver();

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
