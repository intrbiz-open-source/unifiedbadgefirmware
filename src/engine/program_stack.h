/*
 * LED program global stack
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef PROGRAM_STACK_H
#define PROGRAM_STACK_H

/*
 * Global stack
 */
#define STACK_MAX 64

typedef struct {
  uint8_t  counter;
  uint8_t  entries[STACK_MAX];
} Stack;

void resetStack();
void pushStack(uint8_t value);
uint8_t popStack();
uint8_t getStackEntries();

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
