/*
 * LED program macro language
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef PROGRAM_H
#define PROGRAM_H

#include "op_codes.h"


/*
 * Op Code Macro Language
 */


/*
 * A no operation
 */
#define NOP OP_NOP,


/*
 * WAIT( register )
 *  
 * Pause execution of the LED script, for the duration specified in the given register
 * 
 * if register == WAIT_RANDOM then execution will pause for a random period of time
 * 
 */
#define WAIT(r)          ((r << OP_M_ROLL) | OP_WAIT),


/*
 * SLEEP( time )
 * 
 * Sleep for the number of given seconds. Time over 5 seconds will place the CPU into power down and turn all LEDs off.
 */
#define SLEEP(t)   ((TARGET_SLEEP << OP_M_ROLL) | OP_CALL), t,


/*
 * SET( register, value )
 * 
 * Set the given register to the given value
 * 
 */
#define SET(r, v)    ((r << OP_M_ROLL) | OP_SET), v,


/*
 * INC( register, value )
 * 
 * Increment (or decrement) the given register by the given value
 * 
 */
#define INC(r, v)     ((r << OP_M_ROLL) | OP_INC), ((uint8_t) v),


/*
 * PUSH( register )
 * 
 * Push the value in the from a local (REG_L*) register onto the argument stack
 */
#define PUSH(r)   ((r << OP_M_ROLL) | OP_PUSH),


/*
 * PUSH_LITERAL( literal )
 * 
 * Push the literal onto the argument stack
 */
#define PUSH_LITERAL(l)   ((PUSH_NEXT << OP_M_ROLL) | OP_PUSH), l,


/*
 * POP( register )
 * 
 * Pop from the argument stack into the register (REG_*)
 */
#define POP(r) ((r << OP_M_ROLL) | OP_POP),


/*
 * CALL_PATTERN( pattern )
 * 
 * Call a built in LED pattern
 * 
 */
#define CALL_PATTERN(p)   ((TARGET_BUILT_IN_LED << OP_M_ROLL) | OP_CALL), p,

#define PATTERN_ALL BUILT_IN_LED_ALL_ON
#define PATTERN_OFF BUILT_IN_LED_ALL_OFF
#define PATTERN_FLASH_RANDOM BUILT_IN_LED_FLASH_RANDOM


/*
 * CALL_SUB( index, name )
 * 
 * Call a subprogram, subfunction or subinclude (by index)
 * 
 */
#define CALL_RANDOM_SUBPROGRAM   ((TARGET_PROGRAM << OP_M_ROLL) | OP_CALL), BUILT_IN_SUBPROGRAM_RANDOM,
#define CALL_SUB(p, n)   ((TARGET_SUBPROGRAM << OP_M_ROLL) | OP_CALL), p,


/* 
 * Call with arguments 
 * 
 * Note arguments have to be pushed in reverse order to the pop operations, as a convention the left to right order here will match the sequential order the subprogram will pop in.
 * 
 * When manually PUSHing remember to push the last argument first
 */
#define CALL_SUB_L(p, n, a)      PUSH_LITERAL(a)\
                                 CALL_SUB(p, n)

#define CALL_SUB_LL(p, n, a, b)  PUSH_LITERAL(b)\
                                 PUSH_LITERAL(a)\
                                 CALL_SUB(p, n)

#define CALL_SUB_LR(p, n, a, b)  PUSH(b)\
                                 PUSH_LITERAL(a)\
                                 CALL_SUBP(p, n)

#define CALL_SUB_R(p, n, a)      PUSH(a)\
                                 CALL_SUB(p, n)

#define CALL_SUB_RL(p, n, a, b)  PUSH_LITERAL(b)\
                                 PUSH(a)\
                                 CALL_SUB(p, n)

#define CALL_SUB_RR(p, n, a, b)  PUSH(b)\
                                 PUSH(a)\
                                 CALL_SUB(p, n)

                                        
/*
 * CALL_PROGRAM( index, name )
 * 
 * Call another program (by index)
 * 
 */
#define CALL_RANDOM_PROGRAM   ((TARGET_PROGRAM << OP_M_ROLL) | OP_CALL), BUILT_IN_PROGRAM_RANDOM,
#define CALL_PROGRAM(p, n)   ((TARGET_PROGRAM << OP_M_ROLL) | OP_CALL), p,


/*
 * ALU_COMMAND( dest_register, source_register, operation )
 * 
 * Pack an Logic / Math command
 */
#define ALU_COMMAND(d, s, o) ((o << BUILT_IN_OP_ROLL) | (d << BUILT_IN_TARGET_ROLL) | s)


/*
 * Logic commands
 */
#define LOGIC_COPY(d, s)  ((TARGET_BUILT_IN_LOGIC << OP_M_ROLL) | OP_CALL), ALU_COMMAND(d, s, BUILT_IN_LOGIC_OP_COPY),
#define LOGIC_NOT(d, s)   ((TARGET_BUILT_IN_LOGIC << OP_M_ROLL) | OP_CALL), ALU_COMMAND(d, s, BUILT_IN_LOGIC_OP_NOT),
#define LOGIC_AND(d, s)   ((TARGET_BUILT_IN_LOGIC << OP_M_ROLL) | OP_CALL), ALU_COMMAND(d, s, BUILT_IN_LOGIC_OP_AND),
#define LOGIC_OR(d, s)    ((TARGET_BUILT_IN_LOGIC << OP_M_ROLL) | OP_CALL), ALU_COMMAND(d, s, BUILT_IN_LOGIC_OP_OR),


/*
 * Math commands
 */
#define MATH_ADD(d, s)  ((TARGET_BUILT_IN_MATH << OP_M_ROLL) | OP_CALL), ALU_COMMAND(d, s, BUILT_IN_MATH_OP_ADD),
#define MATH_SUB(d, s)  ((TARGET_BUILT_IN_MATH << OP_M_ROLL) | OP_CALL), ALU_COMMAND(d, s, BUILT_IN_MATH_OP_SUB),
#define MATH_MUL(d, s)  ((TARGET_BUILT_IN_MATH << OP_M_ROLL) | OP_CALL), ALU_COMMAND(d, s, BUILT_IN_MATH_OP_MUL),
#define MATH_DIV(d, s)  ((TARGET_BUILT_IN_MATH << OP_M_ROLL) | OP_CALL), ALU_COMMAND(d, s, BUILT_IN_MATH_OP_DIV),


/*
 * MATRIX_ON( line ) 
 * 
 * Set a matrix line On, one of: C* or R*
 * 
 */
#define MATRIX_ON(l)     ((l << OP_S_ROLL) | OP_LINE_ON),


/*
 * MATRIX_OFF( line ) 
 * 
 * Set matrix line Off, one of: C* or R*
 * 
 */
#define MATRIX_OFF(l)     ((l << OP_S_ROLL) | OP_LINE_OFF),


/*
 * PWM_SET( line, value ) 
 * 
 * Set the PWM level for the matrix line (one of: C* or R*) to value (PWM_OFF to PWM_ON)
 * 
 */
#define PWM_SET(l, v)     ((l << OP_S_ROLL) | OP_PWM_SET), v,


/*
 * PWM_FADE( line, value ) 
 * 
 * Fade the PWM level for the matrix line (one of: C* or R*) to value (PWM_OFF to PWM_ON) over the current period
 * 
 */
#define PWM_FADE(l, v)     ((l << OP_S_ROLL) | OP_PWM_FADE), v,


/*
 * FLAH_LED( led ) 
 * 
 * Flash the given LED, one of: D* (or nice names)
 * 
 */
#define FLASH_LED(l)     ((l << OP_XS_ROLL) | OP_FLASH),


/*
 * START_LOOP( loop_id, iterations )
 * 
 * Define the start of a LED script loop
 * - loop_id: the loop id, one of: LOOP_*
 * - iterations: the number of loop iterations (max 250)
 * 
 */
#define START_LOOP(i,l)  ((i << OP_L_ROLL) | OP_LOOP_START), l,


/*
 * END_LOOP( loop_id )
 * 
 * Define the end of a LED script loop
 * - loop_id: the loop id (must match with START_LOOP), one of: LOOP_*
 * 
 */
#define END_LOOP(i)     ((i << OP_L_ROLL) | OP_LOOP_END),


/*
 * JUMP_SHORT( register, operator, value, target )
 * 
 * Perform a short jump, where:
 * - register is the register (REG_*) to check
 * - operator is the comparision operation (JUMP_OP_*)
 * - value is the expected value
 * - target is the number of bytes to move forward the program counter (1 to 4)
 * 
 */
#define JUMP_SHORT(r, o, v, t) ((r << OP_M_ROLL) | OP_JUMP), ((t << JUMP_TARGET_ROLL) | JUMP_MODE_SRT | (o & JUMP_OP_MASK)), v,

#define JUMP_FORWARD(r, o, v, t)  ((r << OP_M_ROLL) | OP_JUMP), (((((uint8_t)(t >> 8)) & 0x03) << JUMP_TARGET_ROLL) | JUMP_MODE_FWD | (o & JUMP_OP_MASK)), v, t,

#define JUMP_BACKWARD(r, o, v, t) ((r << OP_M_ROLL) | OP_JUMP), (((((uint8_t)(t >> 8)) & 0x03) << JUMP_TARGET_ROLL) | JUMP_MODE_BWD | (o & JUMP_OP_MASK)), v, t,

#define JUMP_ABSOLUTE(r, o, v, t) ((r << OP_M_ROLL) | OP_JUMP), (((((uint8_t)(t >> 8)) & 0x03) << JUMP_TARGET_ROLL) | JUMP_MODE_ABS | (o & JUMP_OP_MASK)), v, t,

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;

