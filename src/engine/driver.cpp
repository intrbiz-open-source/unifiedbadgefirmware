/*
 * LED driver implementation
 * 
 * Copyright - Chris Ellis - 2023
 */

#include <Arduino.h>
#include <avr/sleep.h>
#include <avr/io.h>
#include <EEPROM.h>
#include "driver.h"

/* Prototypes */
static inline void yeild();
#ifdef PIN_S1
static inline void setupS1();
static inline void unsetupS1();
#endif
static inline void setupRTC(void);
static inline void setupUnused();
static inline void setupMatrix();
static void seedRandom();
#ifdef SENSOR_LIGHT
static inline void sampleLightLevel();
#endif
#ifdef PIN_WO0
static inline void analogWriteWO0(uint8_t duty);
#endif
#ifdef PIN_WO1
static inline void analogWriteWO1(uint8_t duty);
#endif
#ifdef PIN_WO2
static inline void analogWriteWO2(uint8_t duty);
#endif
#ifdef PIN_WO3
static inline void analogWriteWO3(uint8_t duty);
#endif
#ifdef PIN_WO4
static inline void analogWriteWO4(uint8_t duty);
#endif
#ifdef PIN_WO5
static inline void analogWriteWO5(uint8_t duty);
#endif
static inline void allLEDsOffFast();

#define BRIGHTNESS_CFG_ADDRESS 0x00

/* Low level PWM handlers */

#ifdef PIN_WO0
static inline void analogWriteWO0(uint8_t duty)
{
    if (duty == PWM_OFF)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_LCMP0EN_bm;
        digitalWriteFast(PIN_WO0, LOW);
    } 
    else if (duty == PWM_ON)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_LCMP0EN_bm;
        digitalWriteFast(PIN_WO0, HIGH);
    }
    else
    {
        TCA0.SPLIT.LCMP0  =  duty;                
        TCA0.SPLIT.CTRLB |=  TCA_SPLIT_LCMP0EN_bm;
    }
}
#endif

#ifdef PIN_WO1
static inline void analogWriteWO1(uint8_t duty)
{
    if (duty == PWM_OFF)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_LCMP1EN_bm;
        digitalWriteFast(PIN_WO1, LOW);
    }
    else if (duty == PWM_ON)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_LCMP1EN_bm;
        digitalWriteFast(PIN_WO1, HIGH);
    }
    else
    {
        TCA0.SPLIT.LCMP1  =  duty;                
        TCA0.SPLIT.CTRLB |=  TCA_SPLIT_LCMP1EN_bm;
    }
}
#endif

#ifdef PIN_WO2
static inline void analogWriteWO2(uint8_t duty)
{
    if (duty == PWM_OFF)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_LCMP2EN_bm;
        digitalWriteFast(PIN_WO2, LOW);
    }
    else if (duty == PWM_ON)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_LCMP2EN_bm;
        digitalWriteFast(PIN_WO2, HIGH);
    }
    else
    {
        TCA0.SPLIT.LCMP2  =  duty;                
        TCA0.SPLIT.CTRLB |=  TCA_SPLIT_LCMP2EN_bm;
    }
}
#endif

#ifdef PIN_WO3
static inline void analogWriteWO3(uint8_t duty)
{
    if (duty == PWM_OFF)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_HCMP0EN_bm;
        digitalWriteFast(PIN_WO3, LOW);
    }
    else if (duty == PWM_ON)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_HCMP0EN_bm;
        digitalWriteFast(PIN_WO3, HIGH);
    }
    else
    {
        TCA0.SPLIT.HCMP0  =  duty;                
        TCA0.SPLIT.CTRLB |=  TCA_SPLIT_HCMP0EN_bm;
    }
}
#endif

#ifdef PIN_WO4
static inline void analogWriteWO4(uint8_t duty)
{
    if (duty == PWM_OFF)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_HCMP1EN_bm;
        digitalWriteFast(PIN_WO4, LOW);
        
    } 
    else if (duty == PWM_ON)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_HCMP1EN_bm;
        digitalWriteFast(PIN_WO4, HIGH);
    }
    else
    {
        TCA0.SPLIT.HCMP1  =  duty;                
        TCA0.SPLIT.CTRLB |=  TCA_SPLIT_HCMP1EN_bm;
    }
}
#endif

#ifdef PIN_WO5
static inline void analogWriteWO5(uint8_t duty)
{
    if (duty == PWM_OFF)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_HCMP2EN_bm;
        digitalWriteFast(PIN_WO5, LOW);
    } 
    else if (duty == PWM_ON)
    {
        TCA0.SPLIT.CTRLB &= ~TCA_SPLIT_HCMP2EN_bm;
        digitalWriteFast(PIN_WO5, HIGH);
    }
    else
    {
        TCA0.SPLIT.HCMP2  =  duty;                
        TCA0.SPLIT.CTRLB |=  TCA_SPLIT_HCMP2EN_bm;
    }
}
#endif

static inline void allLEDsOffFast()
{
    /* Low level turn all off */
    writeMatrixC0(PWM_OFF);
    #if MATRIX_COL_MAX > 1
    writeMatrixC1(PWM_OFF);
    #endif
    #if MATRIX_COL_MAX > 2
    writeMatrixC2(PWM_OFF);
    #endif
    #if MATRIX_COL_MAX > 3
    writeMatrixC3(PWM_OFF);
    #endif
    #if MATRIX_COL_MAX > 4
    writeMatrixC4(PWM_OFF);
    #endif
    #if MATRIX_COL_MAX > 5
    writeMatrixC5(PWM_OFF);
    #endif
    writeMatrixR0(HIGH);
    #if MATRIX_ROW_MAX > 1
    writeMatrixR1(HIGH);
    #endif
    #if MATRIX_ROW_MAX > 2
    writeMatrixR2(HIGH);
    #endif
    #if MATRIX_ROW_MAX > 3
    writeMatrixR3(HIGH);
    #endif
    #if MATRIX_ROW_MAX > 4
    writeMatrixR4(HIGH);
    #endif
    #if MATRIX_ROW_MAX > 5
    writeMatrixR5(HIGH);
    #endif
    #if MATRIX_ROW_MAX > 6
    writeMatrixR6(HIGH);
    #endif
    #if MATRIX_ROW_MAX > 7
    writeMatrixR7(HIGH);
    #endif
    #if MATRIX_ROW_MAX > 8
    writeMatrixR8(HIGH);
    #endif
    #if MATRIX_ROW_MAX > 9
    writeMatrixR9(HIGH);
    #endif
}

/* Generic driver implementation */

/*
 * Brightness management
 */
static uint8_t brightnessScale = BRIGHTNESS_FACTOR_DEFAULT;

uint8_t scaleBrightness(uint8_t brightness)
{
  // Clamp the brightness
  if (brightness > BRIGHT)
    brightness = BRIGHT;
  if (brightness < DIM)
    brightness = DIM;
  // Compute the PWM level
  return (brightnessScale - 1) + ((brightness - 1) * brightnessScale);
}

uint8_t nextRandom(uint8_t min, uint8_t max)
{
    return random(min, max);
}

/*
 * Generic wait during program execution
 */
void wait(uint8_t period)
{
    /* Randomise the period */
    if (period == WAIT_RANDOM)
      period = nextRandom(RANDOM_WAIT_MIN, RANDOM_WAIT_MAX);
    /* Wait */
    for (uint8_t waitCounter = period; waitCounter > 0; waitCounter--)
    {
        delay(PERIOD_RESOLUTION);
        // check abort
        if (isAbort())
          break;
    }
    /* Process any background tasks */
    yeild();
}


/*
 * Place CPU into low power sleep
 */
void sleep_wait(uint8_t seconds)
{
  while (seconds-- > 0)
  {
    sleep_cpu();
    // sample randomness
    seedRandom();
    // flash a random LED
    #ifdef FLASH_DURING_SLEEP
      builtInLedFlashRandom(scaleBrightness(MID), 2);
    #endif
    //check abort
    if (isAbort())
      break;
  }
}

/*
 * PWM value shadow state
 */
static uint8_t lineValue[LINE_MAX];

/*
 * Write to a LED matrix row or column, setting the brightness to the given value
 */
void writeLine(uint8_t line, uint8_t value)
{
    // Update our shadow state
    lineValue[line] = value;
    // set the line
    switch (line)
    {
        /* Rows */
        case R0:
            writeMatrixR0((value == PWM_OFF) ? HIGH : LOW);
            break;
        case R1:
            #if MATRIX_ROW_MAX > 1
            writeMatrixR1((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        case R2:
            #if MATRIX_ROW_MAX > 2
            writeMatrixR2((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        case R3:
            #if MATRIX_ROW_MAX > 3
            writeMatrixR3((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        case R4:
            #if MATRIX_ROW_MAX > 4
            writeMatrixR4((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        case R5:
            #if MATRIX_ROW_MAX > 5
            writeMatrixR5((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        case R6:
            #if MATRIX_ROW_MAX > 6
            writeMatrixR6((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        case R7:
            #if MATRIX_ROW_MAX > 7
            writeMatrixR7((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        case R8:
            #if MATRIX_ROW_MAX > 8
            writeMatrixR8((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        case R9:
            #if MATRIX_ROW_MAX > 9
            writeMatrixR9((value == PWM_OFF) ? HIGH : LOW);
            #endif
            break;
        /* Cols */
        case C0:
            writeMatrixC0(value);
            break;
        case C1:
            #if MATRIX_COL_MAX > 1
            writeMatrixC1(value);
            #endif
            break;
        case C2:
            #if MATRIX_COL_MAX > 2
            writeMatrixC2(value);
            #endif
            break;
        case C3:
            #if MATRIX_COL_MAX > 3
            writeMatrixC3(value);
            #endif
            break;
        case C4:
            #if MATRIX_COL_MAX > 4
            writeMatrixC4(value);
            #endif
            break;
        case C5:
            #if MATRIX_COL_MAX > 5
            writeMatrixC5(value);
            #endif
            break;
    }
}

void refreshLines()
{
  for (uint8_t line = 0; line < LINE_MAX; line++)
  {
    writeLine(line, lineValue[line]);
  }
}

void fadeLine(uint8_t line, uint8_t value, uint8_t period)
{
    // TODO
}

/*
 * Flash LED on and off by row number
 */
void flashLed(uint8_t col, uint8_t row, uint8_t value, uint8_t period)
{
  writeLine(col, value);
  writeLine(row, value);
  wait(period);
  writeLine(row, PWM_OFF);
  writeLine(col, PWM_OFF);
  wait(period);
}

/*
 * Flash and LED on and off
 */
void flashLed(uint8_t led, uint8_t value, uint8_t period)
{
  if (led < LED_MAX)
  {
    /* Translate the LED to row and col */
    uint8_t col = leds[led].col;
    uint8_t row = leds[led].row;
    /* Flash the LED */
    writeLine(col, value);
    writeLine(row, value);
    wait(period);
    writeLine(row, PWM_OFF);
    writeLine(col, PWM_OFF);
    wait(period);
  }
}

/*
 * Turn all LEDs on
 */
void builtInLedAllOn(uint8_t value, uint8_t period)
{
  // enable all rows
  for (uint8_t row = R0; row < (R0 + MATRIX_ROW_MAX); row++)
  {
    writeLine(row, PWM_ON);
  }
  // strobe the columns
  for (uint16_t f = ((period * PERIOD_RESOLUTION) / (MATRIX_COL_MAX * 2)); f > 0; f--)
  {
    for (uint8_t col = C0; col < (C0 + MATRIX_COL_MAX); col++)
    {
        writeLine(col, value);
        delay(2);
        writeLine(col, PWM_OFF);
    }
  }
  // disable all the rows
  for (uint8_t row = 0; row < MATRIX_ROW_MAX; row++)
  {
    writeLine(row, PWM_OFF);
  }
}


/*
 * Turn all LEDs off
 */
void builtInLedAllOff()
{
    /* Low level turn all off */
    allLEDsOffFast();
}


/*
 * Flash a random LED
 */
void builtInLedFlashRandom(uint8_t value, uint8_t period)
{
    flashLed(nextRandom(D0, D59 + 1), value, period);
}

/*
 * Custom patterns
 */
void builtInLedCustom(uint8_t target, uint8_t value, uint8_t period)
{
  // TODO: support per variant custom patterns
}


static inline void yeild()
{
  // Process any sensors
  #ifdef SENSOR_LIGHT
    sampleLightLevel();
  #endif
}

#ifdef SENSOR_LIGHT
static int lightLevel = 0;

static inline void sampleLightLevel()
{
  int s1 = analogRead( SENSOR_LIGHT );
  delay(10);
  int s2 = analogRead( SENSOR_LIGHT );
  delay(10);
  int s3 = analogRead( SENSOR_LIGHT );
  delay(10);
  int s4 = analogRead( SENSOR_LIGHT );
  // average the samples
  int avg = (s1 + s2 + s3 + s4) / 4;
  // moving average with previous value
  lightLevel = (lightLevel == 0) ? avg : ((lightLevel + avg) / 2);
  // better way to map?
  brightnessScale = 2 + (lightLevel / 16);
}
#endif

/*
 * Random number seeding
 */
static uint8_t seeded = 0;

static void seedRandom()
{
  #if defined SEED_UNUSED_1 || defined SEED_S1
    if (seeded == 0)
    {
      /* Swap unused pins to input */
      #ifdef SEED_UNUSED_1
        pinModeFast(UNUSED_1, INPUT);
        #define SEED_1 UNUSED_1
        #ifdef SEED_UNUSED_2
          pinModeFast(UNUSED_2, INPUT);
          #define SEED_2 UNUSED_2
        #else
          #define SEED_2 UNUSED_1
        #endif
      #endif
      #ifdef SEED_S1
        unsetupS1();
        pinModeFast(PIN_S1, INPUT);
        #define SEED_1 PIN_S1
        #define SEED_2 PIN_S1
      #endif
      /* Sample floated pins */
      for (uint8_t attempt = 10; attempt > 0; attempt--)
      {
        delay(2);
        int s1 = analogRead(SEED_1);
        delay(2);
        int s2 = analogRead(SEED_2);
        delay(2);
        int s3 = analogRead(SEED_1);
        delay(2);
        int s4 = analogRead(SEED_2);
        /* Seed random */
        long seed = ((s1 & 0xF) << 12) | ((s2 & 0xF) << 8) | ((s3 & 0xF) << 4) | (s4 & 0xF);
        if (seed != 0x00 && __builtin_popcount(seed) > 3)
        {
          randomSeed(seed);
          seeded = 1;
          break;
        }
      }
      /* Unfloat spare pins */
      #ifdef SEED_UNUSED_1
        pinModeFast(UNUSED_1, OUTPUT);
        digitalWriteFast(UNUSED_1, LOW);
      #endif
      #ifdef SEED_UNUSED_2
        pinModeFast(UNUSED_2, OUTPUT);
        digitalWriteFast(UNUSED_2, LOW);
      #endif
      #ifdef SEED_S1
        setupS1();
      #endif
    }
    else
    {
      // rely on wraparound
      seeded++;
    }
  #else
    #warning "Using device serial number for random seed"
    // By default use the device serial number
    if (seeded == 0)
    {
      long seed = ((SIGROW_SERNUM0 & 0xF) << 12) | ((SIGROW_SERNUM1 & 0xF) << 8) | ((SIGROW_SERNUM2 & 0xF) << 4) | (SIGROW_SERNUM3 & 0xF);
      if (seed != 0x00 && __builtin_popcount(seed) > 3)
      {
        randomSeed(seed);
      }
      seeded = 1;
    }
  #endif
}

/*
 * Setup RTC to enable SLEEP
 */
static inline void setupRTC(void)
{
  while (RTC.STATUS > 0) { ; }
  RTC.CLKSEL = RTC_CLKSEL_INT32K_gc;
  RTC.PITINTCTRL = RTC_PI_bm;
  /* Trigger PIT interrupt at 1Hz, so the CPU will come out of SLEEP every second */
  RTC.PITCTRLA = RTC_PERIOD_CYC32768_gc | RTC_PITEN_bm;
}

/* RTC Wakeup From SLEEP ISR */
ISR(RTC_PIT_vect)
{
  /* Clear interrupt */
  RTC.PITINTFLAGS = RTC_PI_bm;
}

#ifdef PIN_S1
static inline void setupS1()
{
  pinModeFast(PIN_S1, INPUT_PULLUP);
  // setup interrupts
  S1_PINCTL |= PORT_ISC_FALLING_gc;
}

static inline void unsetupS1()
{
  // disable interrupts
  S1_PINCTL &= ~PORT_ISC_gm;
}

ISR(S1_VECTOR)
{
  /* Clear the interrupt */
  S1_FLAGS = 0xFF;
  /* Disable interrupts */
  S1_PINCTL &= ~PORT_ISC_gm;
  /* Process the interrupt */
  // lazy debounce
  delay(30);
  // force all LEDs off low level
  allLEDsOffFast();
  bool restore = true;
  // time the button press
  uint8_t hold = 0;
  while (digitalReadFast(PIN_S1) == LOW)
  {
    delay(500);
    if (hold == 0)
    {
      /* Short press */
      // abort the current program
      setAbort();
      restore = false;
    }
    else
    {
      /* Scroll through the brightness levels */
      // increment the brightness
      brightnessScale += BRIGHTNESS_FACTOR_STEP;
      if (brightnessScale > BRIGHTNESS_FACTOR_MAX)
        brightnessScale = BRIGHTNESS_FACTOR_MIN;
      // save scale in EEPROM
      EEPROM.write(BRIGHTNESS_CFG_ADDRESS, brightnessScale);
      // display the new brightness levels
      uint8_t value = scaleBrightness(BRIGHT);
      displayBrightnessOn(value);
      delay(500);
      displayBrightnessOff();
      delay(500);
    }
    hold++;
  }
  // Restore the lines
  if (restore)
    refreshLines();
  /* Reenable interrupts*/
  S1_PINCTL |= PORT_ISC_FALLING_gc;
}
#endif

static inline void setupUnused()
{
  // init unused pins
  #ifdef UNUSED_1
    pinModeFast(UNUSED_1, OUTPUT);
    digitalWriteFast(UNUSED_1, LOW);
  #endif
  #ifdef UNUSED_2
    pinModeFast(UNUSED_2, OUTPUT);
    digitalWriteFast(UNUSED_2, LOW);
  #endif
  #ifdef UNUSED_3
    pinModeFast(UNUSED_3, OUTPUT);
    digitalWriteFast(UNUSED_3, LOW);
  #endif
  #ifdef UNUSED_4
    pinModeFast(UNUSED_4, OUTPUT);
    digitalWriteFast(UNUSED_4, LOW);
  #endif
  #ifdef UNUSED_5
    pinModeFast(UNUSED_5, OUTPUT);
    digitalWriteFast(UNUSED_5, LOW);
  #endif
  #ifdef UNUSED_INPUT_1
    pinModeFast(UNUSED_INPUT_1, INPUT);
  #endif
  #ifdef UNUSED_INPUT_2
    pinModeFast(UNUSED_INPUT_2, INPUT);
  #endif
  #ifdef UNUSED_INPUT_3
    pinModeFast(UNUSED_INPUT_3, INPUT);
  #endif
  #ifdef UNUSED_INPUT_4
    pinModeFast(UNUSED_INPUT_4, INPUT);
  #endif
  #ifdef UNUSED_INPUT_5
    pinModeFast(UNUSED_INPUT_5, INPUT);
  #endif
}

static inline void setupMatrix()
{
  // setup PWM
  #ifdef MILLIS_USE_TIMERA0
    #error "This takes over TCA0 - please use a different timer for millis"
  #endif
  takeOverTCA0();
  TCA0.SINGLE.CTRLD = 0x01;
  TCA0.SPLIT.LPER   = PWM_TOP;
  TCA0.SPLIT.HPER   = PWM_TOP;
  TCA0.SPLIT.CTRLA  = TCA_SPLIT_CLKSEL_DIV8_gc | TCA_SPLIT_ENABLE_bm;
  /* 
   * Column lines
   */
  #ifdef PIN_WO0
    pinModeFast(PIN_WO0, OUTPUT);
    digitalWriteFast(PIN_WO0, LOW);
    analogWriteWO0(PWM_OFF);
  #endif
  #ifdef PIN_WO1
    pinModeFast(PIN_WO1, OUTPUT);
    digitalWriteFast(PIN_WO1, LOW);
    analogWriteWO1(PWM_OFF);
  #endif
  #ifdef PIN_WO2
    pinModeFast(PIN_WO2, OUTPUT);
    digitalWriteFast(PIN_WO2, LOW);
    analogWriteWO2(PWM_OFF);
  #endif
  #ifdef PIN_WO3
    pinModeFast(PIN_WO3, OUTPUT);
    digitalWriteFast(PIN_WO3, LOW);
    analogWriteWO3(PWM_OFF);
  #endif
  #ifdef PIN_WO4
    pinModeFast(PIN_WO4, OUTPUT);
    digitalWriteFast(PIN_WO4, LOW);
    analogWriteWO4(PWM_OFF);
  #endif
  #ifdef PIN_WO5
    pinModeFast(PIN_WO5, OUTPUT);
    digitalWriteFast(PIN_WO5, LOW);
    analogWriteWO5(PWM_OFF);
  #endif
  /*
   * Row lines
   */
  #ifdef PIN_R0
    pinModeFast(PIN_R0, OUTPUT);
    digitalWriteFast(PIN_R0, HIGH);
  #endif
  #ifdef PIN_R1
    pinModeFast(PIN_R1, OUTPUT);
    digitalWriteFast(PIN_R1, HIGH);
  #endif
  #ifdef PIN_R2
    pinModeFast(PIN_R2, OUTPUT);
    digitalWriteFast(PIN_R2, HIGH);
  #endif
  #ifdef PIN_R3
    pinModeFast(PIN_R3, OUTPUT);
    digitalWriteFast(PIN_R3, HIGH);
  #endif
  #ifdef PIN_R4
    pinModeFast(PIN_R4, OUTPUT);
    digitalWriteFast(PIN_R4, HIGH);
  #endif
  #ifdef PIN_R5
    pinModeFast(PIN_R5, OUTPUT);
    digitalWriteFast(PIN_R5, HIGH);
  #endif
  #ifdef PIN_R6
    pinModeFast(PIN_R6, OUTPUT);
    digitalWriteFast(PIN_R6, HIGH);
  #endif
  #ifdef PIN_R7
    pinModeFast(PIN_R7, OUTPUT);
    digitalWriteFast(PIN_R7, HIGH);
  #endif
  #ifdef PIN_R8
    pinModeFast(PIN_R8, OUTPUT);
    digitalWriteFast(PIN_R8, HIGH);
  #endif
  #ifdef PIN_R9
    pinModeFast(PIN_R9, OUTPUT);
    digitalWriteFast(PIN_R9, HIGH);
  #endif
    /*
     * Setup the shadow state
     */
    memset(lineValue, 0x00, LINE_MAX);
    /*
     * Setup brightness
     */
    brightnessScale = EEPROM.read(BRIGHTNESS_CFG_ADDRESS);
    if (brightnessScale < BRIGHTNESS_FACTOR_MIN || brightnessScale > BRIGHTNESS_FACTOR_MAX)
      brightnessScale = BRIGHTNESS_FACTOR_DEFAULT;
}

void setupDriver()
{
  // setup unused pins
  setupUnused();
  // setup ADC
  analogSampleDuration(20);
  // setup sensors
  #ifdef SENSOR_LIGHT
    pinModeFast(SENSOR_LIGHT, INPUT);
  #endif
  #ifdef PIN_S1
    setupS1();
  #endif
  // seed the random number generator
  seedRandom();
  // setup LED matrix
  setupMatrix();
  // Setup sleep
  setupRTC();
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);
  sleep_enable();
}
