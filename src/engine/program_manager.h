/*
 * LED program management
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef PROGRAM_MANAGER_H
#define PROGRAM_MANAGER_H

/*
 * Progam defintions
 */
#define START_SUBPROGRAM(id, ...) const uint8_t SUBPROGRAM_##id[] = {
#define END_SUBPROGRAM };

#define START_SUBFUNCTION(id, ...) const uint8_t SUBFUNCTION_##id[] = {
#define END_SUBFUNCTION };

#define START_SUBINCLUDE(id, ...) const uint8_t SUBINCLUDE_##id[] = {
#define END_SUBINCLUDE };

#define START_PROGRAM(id) const uint8_t PROGRAM_##id[] = {
#define END_PROGRAM };

typedef enum {
  MODE_PROGRAM     = 0,
  MODE_SUBPROGRAM  = 1,
  MODE_FUNCTION    = 2,
  MODE_INCLUDE     = 3,
  MODE_SKIP        = 4
} ProgramMode;

typedef struct __attribute__ ((packed)) {
  const uint8_t *ops;
  const uint16_t size;
  const uint8_t /*ProgramMode*/ mode; 
} Program;

#define EMPTY_DEFAULTS {0, 0, 0, 0, 0, 0}

#define START_SUBPROGRAMS  const Program SUBPROGRAMS[] = {

#define SUBPROGRAM(index, id, ...) { SUBPROGRAM_##id, sizeof(SUBPROGRAM_##id), (uint8_t) MODE_PROGRAM },
#define SUBFUNCTION(index, id, ...) { SUBFUNCTION_##id, sizeof(SUBFUNCTION_##id), (uint8_t) MODE_FUNCTION },
#define SUBINCLUDE(index, id) { SUBINCLUDE_##id, sizeof(SUBINCLUDE_##id), (uint8_t) MODE_INCLUDE },
#define NOSUB(index) { NULL, 0, (uint8_t) MODE_SKIP },

#define END_SUBPROGRAMS };

#define SUBPROGRAMS_SIZE (sizeof(SUBPROGRAMS) / sizeof(Program))

#define GET_SUBPROGRAM(i) &(SUBPROGRAMS[(i < SUBPROGRAMS_SIZE ? i : 0)])

#define START_PROGRAMS  const Program PROGRAMS[] = {

#define PROGRAM(index, id) { PROGRAM_##id, sizeof(PROGRAM_##id) },

#define END_PROGRAMS };

#define PROGRAMS_SIZE (sizeof(PROGRAMS) / sizeof(Program))

#define GET_PROGRAM(i) &(PROGRAMS[(i < PROGRAMS_SIZE ? i : 0)])

/*
 * Current program management
 */
const Program *currentProgram();
void nextProgram();
void setProgram(uint8_t index);
bool isAbort();
void clearAbort();
void setAbort();

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;

