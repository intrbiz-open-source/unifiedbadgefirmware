/*
 * LED Runtime
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef RUNTIME_H
#define RUNTIME_H

#include "variant.h"
#include "driver.h"
#include "program_state.h"
#include "program_stack.h"

/*
 * Runtime init
 */
void setupRuntime();

/*
 * Runtime loop
 */
void loopRuntime();

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
