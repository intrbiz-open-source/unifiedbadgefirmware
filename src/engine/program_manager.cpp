/*
 * LED program manager
 * 
 * Copyright - Chris Ellis - 2023
 */

#include <Arduino.h>
#include "variant.h"

/* Current program index */
static volatile uint8_t programIndex = 0;
static volatile bool doAbort;

const Program* currentProgram()
{
  return GET_PROGRAM(programIndex);
}

void nextProgram()
{
  programIndex++;
  if (programIndex >= PROGRAMS_SIZE)
    programIndex = 0;
}

void setProgram(uint8_t index)
{
  programIndex = index;
  if (programIndex >= PROGRAMS_SIZE)
    programIndex = 0;
}

bool isAbort()
{
  return doAbort;
}

void clearAbort()
{
  doAbort = false;
}

void setAbort()
{
  doAbort = true;
}

// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
