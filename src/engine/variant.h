/*
 * LED device variant
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef ENGINE_VARIANT_H
#define ENGINE_VARIANT_H

#include "program.h"
#include "program_manager.h"

typedef struct {
  uint8_t col;
  uint8_t row;
} LED;

/* Include the specific variant */

#include "../variants/variants.h"

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
