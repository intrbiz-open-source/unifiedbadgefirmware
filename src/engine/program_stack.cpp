/*
 * LED program shared stack
 * 
 * Copyright - Chris Ellis - 2023
 */

#include <Arduino.h>
#include "program_stack.h"

/* Shared stack */
static Stack stack;

void resetStack()
{
  memset(&stack, 0x00, sizeof(Stack));
}

void pushStack(uint8_t value)
{
  if (stack.counter < STACK_MAX)
  {
    stack.entries[stack.counter] = value;
    stack.counter++;
  }
}

uint8_t popStack()
{
  if (stack.counter > 0)
  {
    return stack.entries[--stack.counter];
  }
  return 0x00;
}

uint8_t getStackEntries()
{
  return stack.counter;
}

// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
