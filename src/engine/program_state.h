/*
 * LED program state structs
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef PROGRAM_STATE_H
#define PROGRAM_STATE_H

typedef struct {
  uint16_t start;
  uint16_t count;
} LoopState;

/* 32 bytes */
typedef struct {
  uint8_t   registers[REGISTER_MAX];
  LoopState loops[LOOPS_MAX];
} ProgramState;

/* Program execution needs about 40 bytes */
#define MAX_DEPTH 20

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
