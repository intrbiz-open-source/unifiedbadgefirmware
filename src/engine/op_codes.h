/*
 * LED Op Codes
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef OP_CODES_H
#define OP_CODES_H


/* Registers */
#define REG_BRIGHTNESS  0
#define REG_PERIOD      1
#define REG_LA          2
#define REG_LB          3
#define REG_LC          4
#define REG_LD          5
#define REG_LE          6
#define REG_LF          7
#define REG_MASK        7
#define REGISTER_MAX    8


/* Decode tree */
#define OP_XS_ROLL      2
#define OP_XS_EXT       0x00
#define OP_XS_LINE      0x01
#define OP_XS_PWM       0x02
#define OP_XS_FLASH     0x03
#define OP_XS_MASK      0x03
#define OP_S_ROLL       3
#define OP_S_LINE_ON    0x04
#define OP_S_PWM_FADE   0x04
#define OP_M_ROLL       5
#define OP_M_PUSH       0x00
#define OP_M_POP        0x01
#define OP_M_SET        0x02
#define OP_M_INC        0x03
#define OP_M_WAIT       0x04
#define OP_M_CALL       0x05
#define OP_M_JUMP       0x06
#define OP_M_LOOP       0x07
#define OP_M_MASK       0x07
#define OP_L_ROLL       6
#define OP_L_LOOP_END   0x20


/* Op Codes */
#define OP_NOP          0x00
#define OP_PUSH         (OP_XS_EXT | (OP_M_PUSH << OP_XS_ROLL))
#define OP_POP          (OP_XS_EXT | (OP_M_POP  << OP_XS_ROLL))
#define OP_SET          (OP_XS_EXT | (OP_M_SET  << OP_XS_ROLL))
#define OP_INC          (OP_XS_EXT | (OP_M_INC  << OP_XS_ROLL))
#define OP_WAIT         (OP_XS_EXT | (OP_M_WAIT << OP_XS_ROLL))
#define OP_CALL         (OP_XS_EXT | (OP_M_CALL << OP_XS_ROLL))
#define OP_JUMP         (OP_XS_EXT | (OP_M_JUMP << OP_XS_ROLL))
#define OP_LOOP_START   (OP_XS_EXT | (OP_M_LOOP << OP_XS_ROLL))
#define OP_LOOP_END     (OP_XS_EXT | (OP_M_LOOP << OP_XS_ROLL) | OP_L_LOOP_END)
#define OP_LINE_OFF     (OP_XS_LINE)
#define OP_LINE_ON      (OP_XS_LINE | OP_S_LINE_ON)
#define OP_PWM_SET      (OP_XS_PWM)
#define OP_PWM_FADE     (OP_XS_PWM  | OP_S_PWM_FADE)
#define OP_FLASH        (OP_XS_FLASH)


/* Loop Names */
#define LOOP_A               0
#define LOOP_B               1
#define LOOP_C               2
#define LOOP_D               3
#define LOOPS_MAX 4


/* Loop Special Values */
#define ITER_BRIGHTNESS  248
#define ITER_PERIOD      249
#define ITER_LA          250
#define ITER_LB          251
#define ITER_LC          252
#define ITER_LD          253
#define ITER_LE          254
#define ITER_LF          255


/*
 * Jump modes
 */
#define JUMP_OP_EQ        0
#define JUMP_OP_LT        1
#define JUMP_OP_GT        2
#define JUMP_OP_NE        3
#define JUMP_OP_LTEQ      4
#define JUMP_OP_GTEQ      5
#define JUMP_OP_ANDCLR    6
#define JUMP_OP_ANDSET    7
#define JUMP_OP_MODCLR    8
#define JUMP_OP_MODSET    9
#define JUMP_OP_MASK      0x0F
#define JUMP_MODE_ABS     0x00
#define JUMP_MODE_FWD     0x10
#define JUMP_MODE_BWD     0x20
#define JUMP_MODE_SRT     0x30
#define JUMP_MODE_MASK    0x30
#define JUMP_TARGET_ROLL  6


/*
 * LED Codes:
 * 0 - 59 - LEDs
 * 60 - Use LA
 * 61 - Use LB
 * 62 - Use LC
 * 63 - Use next byte for the LED no
 */
#define D0         0
#define D1         1
#define D2         2
#define D3         3
#define D4         4
#define D5         5
#define D6         6
#define D7         7
#define D8         8
#define D9         9
#define D10         10
#define D11         11
#define D12         12
#define D13         13
#define D14         14
#define D15         15
#define D16         16
#define D17         17
#define D18         18
#define D19         19
#define D20         20
#define D21         21
#define D22         22
#define D23         23
#define D24         24
#define D25         25
#define D26         26
#define D27         27
#define D28         28
#define D29         29
#define D30         30
#define D31         31
#define D32         32
#define D33         33
#define D34         34
#define D35         35
#define D36         36
#define D37         37
#define D38         38
#define D39         39
#define D40         40
#define D41         41
#define D42         42
#define D43         43
#define D44         44
#define D45         45
#define D46         46
#define D47         47
#define D48         48
#define D49         49
#define D50         50
#define D51         51
#define D52         52
#define D53         53
#define D54         54
#define D55         55
#define D56         56
#define D57         57
#define D58         58
#define D59         59
#define LED_LA      60
#define LED_LB      61
#define LED_LC      62
#define LED_NEXT    63
#define LED_OP_MAX  64


/*
 * Line Codes:
 * 24 - 
 * 25 - Use LA
 * 26 - Use LB
 * 27 - Use LC
 * 28 - Use LD
 * 29 - Use LE
 * 30 - Use LF
 * 31 - Use next byte for the line no
 */
#define R0         0
#define R1         1
#define R2         2
#define R3         3
#define R4         4
#define R5         5
#define R6         6
#define R7         7
#define R8         8
#define R9         9
#define R10        10
#define R11        11
#define R12        12
#define R13        13
#define R14        14
#define R15        15
#define C0         16
#define C1         17
#define C2         18
#define C3         19
#define C4         20
#define C5         21
#define C6         22
#define C7         23
#define LINE_RSV   24
#define LINE_LA    25
#define LINE_LB    26
#define LINE_LC    27
#define LINE_LD    28
#define LINE_LE    29
#define LINE_LF    30
#define LINE_NEXT  31
#define LINE_MAX   32


/* Brightness levels */
#define DIM     2
#define LEVEL_2 3
#define LEVEL_3 4
#define MID     5
#define LEVEL_5 6
#define LEVEL_6 7
#define BRIGHT  8


/*
 * Push special cases
 */
#define PUSH_NOP   0
#define PUSH_NEXT  1


/*
 * Wait special values
 * 0 - Random wait
 */
#define WAIT_RANDOM  0


/*
 * Special call target values:
 * 0 - Call LED built-in
 * 1 - Call colour built-in
 * 2 - Call random built-in
 * 3 - Call logic built-in
 * 4 - Call math built-in
 * 5 - Call sleep built-in
 * 6 - Call subprogram
 * 7 - Call program
 */
#define TARGET_BUILT_IN_LED     0
#define TARGET_BUILT_IN_COLOUR  1
#define TARGET_BUILT_IN_RANDOM  2
#define TARGET_BUILT_IN_LOGIC   3
#define TARGET_BUILT_IN_MATH    4
#define TARGET_SLEEP            5
#define TARGET_SUBPROGRAM       6
#define TARGET_PROGRAM          7

#define BUILT_IN_OP_ROLL 6
#define BUILT_IN_TARGET_ROLL 3

#define BUILT_IN_LOGIC_OP_COPY 0
#define BUILT_IN_LOGIC_OP_NOT  1
#define BUILT_IN_LOGIC_OP_AND  2
#define BUILT_IN_LOGIC_OP_OR   3

#define BUILT_IN_MATH_OP_ADD   0
#define BUILT_IN_MATH_OP_SUB   1
#define BUILT_IN_MATH_OP_MUL   2
#define BUILT_IN_MATH_OP_DIV   3

#define BUILT_IN_LED_ALL_OFF      0
#define BUILT_IN_LED_ALL_ON       1
#define BUILT_IN_LED_FLASH_RANDOM 2

#define BUILT_IN_SUBPROGRAM_RANDOM 255
#define BUILT_IN_PROGRAM_RANDOM 255


#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
