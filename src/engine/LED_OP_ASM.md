
push $register
push literal
pop $register
set $register literal
inc $register literal
wait $register
call target arg arg
jump @label $register > literal
start loop iterations
end loop
off $register
off literal
on $register
on literal
pwm $register literal
pwm literal literal
fade $register literal
fade literal literal
flash $register
flash literal

+include_name
  instruction

.function_name(arg, arg)
  instructions

:subprogram_name(arg, arg)
  instructions

#program
  instructions

  
```
# This is a comment

#
# This is a subinclude
#
include all_rows_off
    off R0
    off R1
    off R2
    off R3
    off R4
    off R5
    off R6
    off R7
    off R8
    off R9


#
# This is a subfunction
#
function rows_all_off
    set $a R0
    loop 10
      off $a
      inc $a 1
    end


#
# This is a subprogram
#
subprogram flash_wing_down
    flash LED_WING_BLUE_1
    flash LED_WING_BLUE_2
    flash LED_WING_BLUE_3
    flash LED_WING_BLUE_4
    flash LED_WING_BLUE_5
    flash LED_WING_BLUE_6
    flash LED_WING_BLUE_7
    flash LED_WING_BLUE_8
    flash LED_WING_BLUE_9
    flash LED_WING_BLUE_10


#
#
#
subprogram  fill_body_down
    on    C0
    on    R0
    wait  $P
    off   R0
    on    R1
    wait  $P
    off   R1
    on    R2
    wait  $P
    off   R2
    on    R3
    wait  $P
    off   R3
    on    R4
    wait  $P
    off   R4
    on    R5
    wait  $P
    off   R5
    on    R6
    wait  $P
    off   R6
    on    R7
    wait  $P
    off   R7
    off   C0


#
# Main example program
#
program main
    call flash_wing_down
    call fill_body_down
```
