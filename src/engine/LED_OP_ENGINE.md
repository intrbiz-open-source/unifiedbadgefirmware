# LED OP Code Engine V3

## OP Code Layout

Operations are encoded with a variable length op code and data.  In most cases fitting in a single byte, however some op codes use subsequent data bytes.  This aims to keep programs as space efficient as possible.

+--------------------------------------------------------------------------------------------------------------------------------------+
| **Command Byte**                                                                                                                     |
+--------------------------------------------------------------------------------------------------------------------------------------+
| Op Len: | L  | M       | S  | XS      |                                                                                              |
+----+----+----+----+----+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
|  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 | OP          | Next Bytes | Description                                         | Decode Tree |
+----+----+----+----+----+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| REGISTER     |  0 |  0 |  0 |  0 |  0 | PUSH        | 0 or 1     | Push to the argument stack                          | 0 -> 0      |
+--------------+----+----+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| REGISTER     |  0 |  0 |  1 |  0 |  0 | POP         | 0          | Pop from the argument stack                         | 0 -> 1      |
+--------------+--------------+----+----+-------------+------------+-----------------------------------------------------+-------------+
| REGISTER     |  0 |  1 |  0 |  0 |  0 | SET         | 1          | Set $REGISTER to the value of the next byte         | 0 -> 2      |
+--------------+--------------+----+----+-------------+------------+-----------------------------------------------------+-------------+
| REGISTER     |  0 |  1 |  1 |  0 |  0 | INC         | 1          | Increment $REGISTER with the next byte              | 0 -> 3      |
+--------------+----+----+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| REGISTER     |  1 |  0 |  0 |  0 |  0 | WAIT        | 0          | Wait for value of $REGISTER                         | 0 -> 4      |
+--------------+----+----+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| TARGET       |  1 |  0 |  1 |  0 |  0 | CALL        | 1          | Call a subprogram, program or built-in              | 0 -> 5      |
+--------------+--------------+----+----+-------------+------------+-----------------------------------------------------+-------------+
| REGISTER     |  1 |  1 |  0 |  0 |  0 | JUMP        | 2 or 3     | Jump                                                | 0 -> 6      |
+---------+----+----+----+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| LOOP ID |  0 |  1 |  1 |  1 |  0 |  0 | LOOP START  | 1          | Start loop $ID, next byte is iterations             | 0 -> 7 -> 0 |
+---------+----+--------------+----+----+-------------+------------+-----------------------------------------------------+-------------+
| LOOP ID |  1 |  1 |  1 |  1 |  0 |  0 | LOOP END    | 0          | End loop                                            | 0 -> 7 -> 1 |
+---------+----+----+----+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| Line No                |  0 |  0 |  1 | OFF         | 0 or 1     | Turn matrix line off                                | 1 -> 0      |
+------------------------+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| Line No                |  1 |  0 |  1 | ON          | 0 or 1     | Turn matrix line on                                 | 1 -> 1      |
+------------------------+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| Line No                |  0 |  1 |  0 | PWM_SET     | 1 or 2     | Set matrix line PWM to next byte                    | 2 -> 0      |
+------------------------+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| Line No                |  1 |  1 |  0 | PWM_FADE    | 1 or 2     | Fade matrix line PWM to next byte                   | 2 -> 1      |
+------------------------+----+----+----+-------------+------------+-----------------------------------------------------+-------------+
| LED No                      |  1 |  1 | FLASH       | 0 or 1     | Flash an LED on and off                             | 3           |
+-----------------------------+----+----+-------------+------------+-----------------------------------------------------+-------------+

## Registers

There are 2 special registers, 6 local registers.

+-------+------------+-------+------------------------------+
| Index | Name       | Size  | Purpose                      |
+-------+------------+-------+------------------------------+
|     0 | Brightness | Uint8 | Local register               |
+-------+------------+-------+------------------------------+
|     1 | Period     | Uint8 | Current Wait Period in 50ms  |
+-------+------------+-------+------------------------------+
|     2 | LA         | Uint8 | Local register               |
+-------+------------+-------+------------------------------+
|     3 | LB         | Uint8 | Local register               |
+-------+------------+-------+------------------------------+
|     4 | LC         | Uint8 | Local register               |
+-------+------------+-------+------------------------------+
|     5 | LD         | Uint8 | Local register               |
+-------+------------+-------+------------------------------+
|     6 | LE         | Uint8 | Local register               |
+-------+------------+-------+------------------------------+
|     7 | LF         | Uint8 | Local register               |
+-------+------------+-------+------------------------------+

## The Stack

The stack is 32 bytes deep and has a pointer to the current position.  The stack is global and is used to pass data to programs and subprograms when calling them. It can also be used to return data from subfunctions back to the calling program.

A push operation will push onto the stack and increment the pointer and a pop operation will pop from the stack and decrement the pointer.

The stack is automatically reset at the end of execution of programs and subprograms.

## Special Literals

### Line No

There can be upto 24 matrix lines using single byte encoding.  There are a couple of magic values:

* 24 - Reserved
* 25 - Use LA
* 26 - Use LB
* 27 - Use LC
* 28 - Use LD
* 29 - Use LE
* 30 - Use LF
* 31 - Use next byte for the line no

### Led No

There can be upto 60 LEDs using single byte encoding.  There are a couple of magic values:

* 60 - Use LA
* 61 - Use LB
* 62 - Use LA, LB
* 63 - Use next byte for the LED no

### Loop Id

There are 4 simple loop counters which can be used to run simple loops.  The loop ID is 0 - 3.

### Loop Iteration Special Cases

* 0   - Skip the loop
* 250 - Use LA
* 251 - Use LB
* 252 - Use LC
* 253 - Use LD
* 254 - Use LE
* 255 - Use LF

### Push From Cases

* 0 - NOP
* 1 - Push the next byte (literal) to the stack

### Wait Special Cases

The wait instruction will use the register specified as the wait time, except for:

* 0 - Random wait

### Call Target

Target specified a built-in function to call or another subprogram / program.

* 0 - Call LED built-in
* 1 - Call colour built-in
* 2 - Call random built-in
* 3 - Call logic built-in
* 4 - Call math built-in
* 5 - Call sleep
* 6 - Call subprogram
* 7 - Call program

### Increment Values

The value for an Increment operation is signed, so as to enable decrement also.  If bit 7 is high then the value is decremented from the register else the value is incremented to the register.  This enables a range of -127 to 127 to be incremented or decremented.

## Jump

The jump operation consumes the next two or three bytes: mode, value and optionally target.

### Mode

+---------------------------------------+
| Mode                                  |
+----+----+----+----+----+----+----+----+
|  7 |  6 |  5 |  4 |  3 |  2 |  1 |  0 |
+----+----+----+----+----+----+----+----+
| Target  | Abs/Rel | Operator          |
+---------+---------+-------------------+

#### Operator (3 - 0):

* 0 - EQ
* 1 - LT
* 2 - GT
* 3 - NE
* 4 - LTEQ
* 5 - GTEQ
* 6 - AND clear
* 7 - AND set
* 8 - MOD clear
* 9 - MOD set

#### Absolute / Relative (5 - 4):

* 0 - Absolute jump
* 1 - Relative forward jump
* 2 - Relative backward jump
* 3 - Short jump

#### Target (7 - 6):

For absolute or relative jumps this is the high order two bits of the 10 bit program counter target.

For short jumps this is the program counter increment, and can therefore move forward at most 4 bytes without using an extra byte of program space.

