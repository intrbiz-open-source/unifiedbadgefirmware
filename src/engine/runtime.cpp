/*
 * LED Op Code Engine
 * 
 * Copyright - Chris Ellis - 2023
 */

#include <Arduino.h>
#include "runtime.h"

/* Prototypes */
static inline void builtInLed(uint8_t value, ProgramState *state);
static inline void builtInRandom(uint8_t value, ProgramState *state);
static inline void builtInLogic(uint8_t value, ProgramState *state);
static inline void builtInMath(uint8_t value, ProgramState *state);
static inline void builtInSleep(uint8_t value, ProgramState *state);
static inline void callSubprogram(uint8_t value, ProgramState *state, uint8_t depth);
static inline void callProgram(uint8_t value, ProgramState *state, uint8_t depth);
static inline bool jumpCompare(const uint8_t jumpOp, const uint8_t a, const uint8_t b);
static uint8_t executeProgram(const Program *program, uint8_t depth, ProgramState *state);
static inline uint8_t runProgram(const Program *program, uint8_t depth, uint8_t brightness, uint8_t period);

/* Main Op Code Runtime */

static inline __attribute__((always_inline)) void builtInLed(uint8_t value, ProgramState *state)
{
    switch (value)
    {
        case BUILT_IN_LED_ALL_OFF:
            builtInLedAllOff();
            break;
        case BUILT_IN_LED_ALL_ON:
            builtInLedAllOn(scaleBrightness(state->registers[REG_BRIGHTNESS]), state->registers[REG_PERIOD]);
            break;
        case BUILT_IN_LED_FLASH_RANDOM:
            builtInLedFlashRandom(scaleBrightness(state->registers[REG_BRIGHTNESS]), state->registers[REG_PERIOD]);
            break;
        default:
            builtInLedCustom(value, scaleBrightness(state->registers[REG_BRIGHTNESS]), state->registers[REG_PERIOD]);
            break;
    }
}

static inline __attribute__((always_inline)) void builtInColour(uint8_t value, ProgramState *state)
{
    // TODO
}

static inline __attribute__((always_inline)) void builtInRandom(uint8_t value, ProgramState *state)
{
    uint8_t min = popStack();
    uint8_t max = popStack();
    state->registers[(value & REG_MASK)] = nextRandom(min, max);
}

static inline __attribute__((always_inline)) void builtInLogic(uint8_t value, ProgramState *state)
{
    // Argument: bits 0-2 from, bits 3-5 target, bits 6,7 = op; op 0 = copy, 1 = NOT, 2 = and, 3 = or
    switch ((value >> BUILT_IN_OP_ROLL))
    {
        case BUILT_IN_LOGIC_OP_COPY:
            /* Copy */
            state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] = state->registers[(value & REG_MASK)];
            break;
        case BUILT_IN_LOGIC_OP_NOT:
            /* NOT */
            state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] = ~ state->registers[(value & REG_MASK)];
            break;
        case BUILT_IN_LOGIC_OP_AND:
            /* AND */
            state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] = state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] & state->registers[(value & REG_MASK)];
            break;
        case BUILT_IN_LOGIC_OP_OR:
            /* OR */
            state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] = state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] | state->registers[(value & REG_MASK)];
            break;
    }
}

static inline __attribute__((always_inline)) void builtInMath(uint8_t value, ProgramState *state)
{
    // Argument: bits 0-2 from, bits 3-5 target, bits 6,7 = op; op 0 = add, 1 = sub, 2 = mul, 3 = div
    switch ((value >> BUILT_IN_OP_ROLL))
    {
        case BUILT_IN_MATH_OP_ADD:
            state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] = state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] + state->registers[(value & REG_MASK)];
            break;
        case BUILT_IN_MATH_OP_SUB:
            state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] = state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] - state->registers[(value & REG_MASK)];
            break;
        case BUILT_IN_MATH_OP_MUL:
            state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] = state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] * state->registers[(value & REG_MASK)];
            break;
        case BUILT_IN_MATH_OP_DIV:
            state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] = state->registers[((value >> BUILT_IN_TARGET_ROLL) & REG_MASK)] / state->registers[(value & REG_MASK)];
            break;
    }
}

static inline __attribute__((always_inline)) void builtInSleep(uint8_t value, ProgramState *state)
{
    if (value > 5)
    {
        // go into deep sleep
        sleep_wait(value);
        // restore line states, since PWM will have been shutdown during SLEEP
        refreshLines();
    }
    else
    {
        wait(value * (1000 / PERIOD_RESOLUTION));
    }
}

static inline __attribute__((always_inline)) void callSubprogram(uint8_t value, ProgramState *state, uint8_t depth)
{
    if (depth < MAX_DEPTH && value < SUBPROGRAMS_SIZE)
    {
        const Program *sub = GET_SUBPROGRAM(value);
        if (sub->mode == MODE_INCLUDE)
        {
            // execute the included program with the existing subprogram state
            executeProgram(sub, depth + 1, state);
        }
        else
        {
            // create a new state an run the subprogram
            runProgram(sub, depth + 1, state->registers[REG_BRIGHTNESS], state->registers[REG_PERIOD]);
        }
    }
}

static inline __attribute__((always_inline)) void callProgram(uint8_t value, ProgramState *state, uint8_t depth)
{
    if (depth < MAX_DEPTH && value < PROGRAMS_SIZE)
    {
        // create a new state an run the program
        runProgram(GET_PROGRAM(value), depth + 1, state->registers[REG_BRIGHTNESS], state->registers[REG_PERIOD]);
    }
}

static inline __attribute__((always_inline)) bool jumpCompare(const uint8_t jumpOp, const uint8_t a, const uint8_t b)
{
    switch (jumpOp & JUMP_OP_MASK)
    {
        case JUMP_OP_EQ:
            return a == b;
        case JUMP_OP_LT:
            return a < b;
        case JUMP_OP_GT:
            return a > b;
        case JUMP_OP_NE:
            return a != b;
        case JUMP_OP_LTEQ:
            return a <= b;
        case JUMP_OP_GTEQ:
            return a >= b;
        case JUMP_OP_ANDCLR:
            return (a & b) == 0;
        case JUMP_OP_ANDSET:
            return (a & b) > 0;
        case JUMP_OP_MODCLR:
            return (a % b) == 0;
        case JUMP_OP_MODSET:
            return (a % b) > 0;
    }
    return false;
}

static uint8_t executeProgram(const Program *program, uint8_t depth, ProgramState *state)
{
    /* Check the basics */
    if (program == NULL || state == NULL)
    {
        return 3;
    }
    /* Execute the program */
    const uint8_t *ops = program->ops;
    const uint8_t length = program->size;
    const ProgramMode mode = (ProgramMode) program->mode;
    /* Validate the program */
    if (ops == NULL || length == 0 || mode == MODE_SKIP)
    {
        return 2;
    }
    /* Execute */
    uint8_t op = 0, arg = 0, value = 0, jumpOp;
    uint16_t programCounter = 0, jumpTo = 0;
    while (programCounter < length && (! isAbort()))
    {
        op = ops[programCounter++];
        switch (op & OP_XS_MASK)
        {
            case OP_XS_EXT:
                /* select the ext op */
                switch ((op >> OP_XS_ROLL) & OP_M_MASK)
                {
                    case OP_M_PUSH:
                        arg = op >> OP_M_ROLL;
                        switch (arg)
                        {
                            case PUSH_NOP:
                                // NOP
                                break;
                            case PUSH_NEXT:
                                pushStack(ops[programCounter++]);
                                break;
                            default:
                                pushStack(state->registers[arg]);
                                break;
                        }
                        break;
                    case OP_M_POP:
                        arg = op >> OP_M_ROLL;
                        state->registers[arg] = popStack();
                        break;
                    case OP_M_SET:
                        arg = op >> OP_M_ROLL;
                        value = ops[programCounter++];
                        state->registers[arg] = value;
                        break;
                    case OP_M_INC:
                        arg = op >> OP_M_ROLL;
                        value = ops[programCounter++];
                        state->registers[arg] += ((int8_t) value);
                        break;
                    case OP_M_WAIT:
                        arg = op >> OP_M_ROLL;
                        wait((arg == WAIT_RANDOM) ? WAIT_RANDOM : state->registers[arg]);
                        break;
                    case OP_M_CALL:
                        arg = op >> OP_M_ROLL;
                        value = ops[programCounter++];
                        switch (arg)
                        {
                            case TARGET_BUILT_IN_LED:
                                builtInLed(value, state);
                                break;
                            case TARGET_BUILT_IN_COLOUR:
                                builtInColour(value, state);
                                break;
                            case TARGET_BUILT_IN_RANDOM:
                                builtInRandom(value, state);
                                break;
                            case TARGET_BUILT_IN_LOGIC:
                                builtInLogic(value, state);
                                break;
                            case TARGET_BUILT_IN_MATH:
                                builtInMath(value, state);
                                break;
                            case TARGET_SLEEP:
                                builtInSleep(value, state);
                                break;
                            case TARGET_SUBPROGRAM:
                                callSubprogram(value, state, depth);
                                break;
                            case TARGET_PROGRAM:
                                callProgram(value, state, depth);
                                break;
                        }
                        break;
                    case OP_M_JUMP:
                        arg   = op >> OP_M_ROLL;
                        jumpOp  = ops[programCounter++];
                        value = ops[programCounter++];
                        if (jumpCompare(jumpOp, state->registers[arg], value))
                        {
                            // jump jump jump
                            switch (jumpOp & JUMP_MODE_MASK)
                            {
                                case JUMP_MODE_ABS:
                                    jumpTo = ops[programCounter++];
                                    jumpTo |= (((uint16_t) (jumpOp >> JUMP_TARGET_ROLL)) << 8);
                                    programCounter = jumpTo;
                                    break;
                                case JUMP_MODE_FWD:
                                    jumpTo = ops[programCounter++];
                                    jumpTo |= (((uint16_t) (jumpOp >> JUMP_TARGET_ROLL)) << 8);
                                    programCounter += jumpTo;
                                    break;
                                case JUMP_MODE_BWD:
                                    jumpTo = ops[programCounter++];
                                    jumpTo |= (((uint16_t) (jumpOp >> JUMP_TARGET_ROLL)) << 8);
                                    programCounter -= jumpTo;
                                    break;
                                case JUMP_MODE_SRT:
                                    // short forward jump
                                    programCounter += 1 + (jumpOp >> JUMP_TARGET_ROLL);
                                    break;
                            }
                        }
                        break;
                    case OP_M_LOOP:
                        arg = op >> OP_L_ROLL;
                        // select the loop op
                        if ((op & OP_L_LOOP_END) == OP_L_LOOP_END)
                        {
                            /* loop end */
                            if (--state->loops[arg].count > 0)
                                programCounter = state->loops[arg].start;
                        }
                        else
                        {
                            /* loop start */
                            value = ops[programCounter++];
                            /* Decode special iteration values */
                            switch (value)
                            {
                                case ITER_BRIGHTNESS:
                                    value = state->registers[REG_BRIGHTNESS];
                                    break;
                                case ITER_PERIOD:
                                    value = state->registers[REG_PERIOD];
                                    break;
                                case ITER_LA:
                                    value = state->registers[REG_LA];
                                    break;
                                case ITER_LB:
                                    value = state->registers[REG_LB];
                                    break;
                                case ITER_LC:
                                    value = state->registers[REG_LC];
                                    break;
                                case ITER_LD:
                                    value = state->registers[REG_LD];
                                    break;
                                case ITER_LE:
                                    value = state->registers[REG_LE];
                                    break;
                                case ITER_LF:
                                    value = state->registers[REG_LF];
                                    break;
                            }
                            /* Setup the loop */
                            if (value > 0)
                            {
                                state->loops[arg].count = value;
                                state->loops[arg].start = programCounter;
                            }
                            else
                            {
                                // skip until the end of the loop is hit
                                while (programCounter < length)
                                {
                                    if (ops[programCounter++] == ((arg << OP_L_ROLL) | OP_LOOP_END))
                                        break;
                                }
                            }
                        }
                        break;
                }
                break;
            case OP_XS_LINE:
                arg = op >> OP_S_ROLL;
                /* decode the line */
                switch (arg)
                {
                    case LINE_LA:
                        arg = state->registers[REG_LA];
                        break;
                    case LINE_LB:
                        arg = state->registers[REG_LB];
                        break;
                    case LINE_LC:
                        arg = state->registers[REG_LC];
                        break;
                    case LINE_LD:
                        arg = state->registers[REG_LD];
                        break;
                    case LINE_LE:
                        arg = state->registers[REG_LE];
                        break;
                    case LINE_NEXT:
                        arg = ops[programCounter++];
                        break;
                }
                /* select the line op */
                if ((op & OP_S_LINE_ON) == OP_S_LINE_ON)
                {
                    writeLine(arg, scaleBrightness(state->registers[REG_BRIGHTNESS]));
                }
                else
                {
                    writeLine(arg, PWM_OFF);
                }
                break;
            case OP_XS_PWM:
                arg = op >> OP_S_ROLL;
                /* decode the line */
                switch (arg)
                {
                    case LINE_LA:
                        arg = state->registers[REG_LA];
                        break;
                    case LINE_LB:
                        arg = state->registers[REG_LB];
                        break;
                    case LINE_LC:
                        arg = state->registers[REG_LC];
                        break;
                    case LINE_LD:
                        arg = state->registers[REG_LD];
                        break;
                    case LINE_LE:
                        arg = state->registers[REG_LE];
                        break;
                    case LINE_NEXT:
                        arg = ops[programCounter++];
                        break;
                }
                /* read the value */
                value = ops[programCounter++];
                /* select the pwm op */
                if ((op & OP_S_PWM_FADE) == OP_S_PWM_FADE)
                {
                    fadeLine(arg, value, state->registers[REG_PERIOD]);
                }
                else
                {
                    writeLine(arg, value);
                }
                break;
            case OP_XS_FLASH:
                arg = op >> OP_XS_ROLL;
                /* decode the LED id */
                switch (arg)
                {
                    case LED_LA:
                        flashLed(state->registers[REG_LA], scaleBrightness(state->registers[REG_BRIGHTNESS]), state->registers[REG_PERIOD]);
                        break;
                    case LED_LB:
                        flashLed(state->registers[REG_LB], scaleBrightness(state->registers[REG_BRIGHTNESS]), state->registers[REG_PERIOD]);
                        break;
                    case LED_LC:
                        flashLed(state->registers[REG_LA], state->registers[REG_LB], scaleBrightness(state->registers[REG_BRIGHTNESS]), state->registers[REG_PERIOD]);
                        break;
                    case LED_NEXT:
                        flashLed(ops[programCounter++], scaleBrightness(state->registers[REG_BRIGHTNESS]), state->registers[REG_PERIOD]);
                        break;
                    default:
                        flashLed(arg, scaleBrightness(state->registers[REG_BRIGHTNESS]), state->registers[REG_PERIOD]);
                        break;
                }
                break;
        }
    }
    // Clean up on exit
    if (mode == MODE_PROGRAM || mode == MODE_SUBPROGRAM || depth == 0)
    {
        // ensure all LEDs are reset off
        builtInLedAllOff();
        // reset the stack
        resetStack();
    }
    // Clear abort at root depth
    if (depth == 0 && isAbort())
    {
        clearAbort();
        return 1;
    }
    // normal exit
    return 0;
}

static inline __attribute__((always_inline)) uint8_t runProgram(const Program *program, uint8_t depth, uint8_t brightness, uint8_t period)
{
    // setup the state
    ProgramState state;
    memset(&state, 0x00, sizeof(ProgramState));
    state.registers[REG_BRIGHTNESS] = brightness;
    state.registers[REG_PERIOD] = period;
    // execute the program
    return executeProgram(program, depth, &state);
}

/* Main */

void setupRuntime()
{
    /* Setup the driver */
    setupDriver();
    /* Setup stack */
    resetStack();
    /* Pick random starting program */
    setProgram(nextRandom(0, PROGRAMS_SIZE));
    /* POST */
    #ifdef ENABLE_POST_ALL_FLASH
    for (uint8_t d = D0; d < LED_MAX; d++)
    {
        flashLed(d, scaleBrightness(INITIAL_BRIGHTNESS), 1);
    }
    #endif
    #ifdef ENABLE_POST_ALL_CHASE
    for (uint8_t c = 0; c < MATRIX_COL_MAX; c++)
    {
        writeLine(C0 + c, scaleBrightness(INITIAL_BRIGHTNESS));
        for (uint8_t r = 0; r < MATRIX_ROW_MAX; r++)
        {
            writeLine(R0 + r, scaleBrightness(INITIAL_BRIGHTNESS));
            delay(20);
            writeLine(R0 + r, PWM_OFF);
        }
        writeLine(C0 + c, PWM_OFF);
    }
    #endif
    #ifdef ENABLE_POST_ALL_ON
    builtInLedAllOn(scaleBrightness(INITIAL_BRIGHTNESS), 20);
    #endif
    /* All off */
    builtInLedAllOff();
}

void loopRuntime()
{
    // Run the program
    uint8_t exitCode = runProgram(currentProgram(), 0, INITIAL_BRIGHTNESS, INITIAL_PERIOD);
    // Move to the next program
    nextProgram();
    // Randomise delay
    if (exitCode == 0)
        sleep_wait(nextRandom(RANDOM_SLEEP_MIN, RANDOM_SLEEP_MAX));
}
