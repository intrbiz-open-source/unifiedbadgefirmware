/*
 * Puffin LED Badge
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H


/* Include the common core module with alternative pinouts */
#define CORE_ALT_PINOUT
#define CORE_CUSTOM_BRIGHTNESS_DISPLAY
#include "common/core.h"

/*
 * Customised brightness display routine
 */
#define displayBrightnessOn(value)  writeMatrixC0(value);\
                                    writeMatrixC3(value);\
                                    writeMatrixC4(value);\
                                    writeMatrixR0(LOW);

#define displayBrightnessOff()      writeMatrixC0(PWM_OFF);\
                                    writeMatrixC3(PWM_OFF);\
                                    writeMatrixC4(PWM_OFF);\
                                    writeMatrixR0(HIGH);


/*
 * LED Programs
 */
#include "puffin/puffin_programs.h"


#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
