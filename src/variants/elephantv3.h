/*
 * elephantv3 LED Badge
 * 
 * Copyright - Chris Ellis - 2024
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H

/* Include the common core module */
#define CORE_ALT_PINOUT
#define CORE_CUSTOM_BRIGHTNESS_DISPLAY
#include "common/core.h"

/*
 * Customised brightness display routine
 */
#define displayBrightnessOn(value)  writeMatrixC5(value);\
writeMatrixC4(value);\
writeMatrixR1(LOW);\
writeMatrixR2(LOW);\
writeMatrixR3(LOW);\
writeMatrixR4(LOW);

#define displayBrightnessOff()      writeMatrixC5(PWM_OFF);\
writeMatrixC4(PWM_OFF);\
writeMatrixR1(HIGH);\
writeMatrixR2(HIGH);\
writeMatrixR3(HIGH);\
writeMatrixR4(HIGH);

/*
 * LED Programs
 */
#include "elephantv3/elephantv3_programs.h"

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
