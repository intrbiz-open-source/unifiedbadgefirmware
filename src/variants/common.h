/*
 * Common programs and subprograms
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef VARIANT_COMMON_H
#define VARIANT_COMMON_H

#include "common/subprograms.h"
#include "common/programs.h"

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
