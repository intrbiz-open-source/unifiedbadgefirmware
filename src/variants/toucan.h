/*
 * LED Core Test
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H


/* Include the common core module pinouts */
#define CORE_CUSTOM_BRIGHTNESS_DISPLAY
#include "common/core.h"

/*
 * Customised brightness display routine
 */
#define displayBrightnessOn(value)  writeMatrixC3(value);\
                                    writeMatrixC4(value);\
                                    writeMatrixR7(LOW);\
                                    writeMatrixR8(LOW);\
                                    writeMatrixR9(LOW);

#define displayBrightnessOff()      writeMatrixC3(PWM_OFF);\
                                    writeMatrixC4(PWM_OFF);\
                                    writeMatrixR7(HIGH);\
                                    writeMatrixR8(LOW);\
                                    writeMatrixR9(LOW);

/*
 * LED Programs
 */
#include "toucan/toucan_programs.h"

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
