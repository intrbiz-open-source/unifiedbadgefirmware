/*
 * Elephant design variants
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H


/*
 * Core settings
 */
#define RANDOM_WAIT_MIN 1
#define RANDOM_WAIT_MAX 10
#define RANDOM_SLEEP_MIN 5
#define RANDOM_SLEEP_MAX 15
#define ITER_RANDOM_MIN 1
#define ITER_RANDOM_MAX 30
#define BRIGHTNESS_FACTOR_DEFAULT 16
#define BRIGHTNESS_FACTOR_MIN 4
#define BRIGHTNESS_FACTOR_STEP 4
#define BRIGHTNESS_FACTOR_MAX 32
#define INITIAL_BRIGHTNESS BRIGHT
#define INITIAL_PERIOD 10
#define FLASH_DURING_SLEEP


/*
 * Basic variant metadata
 */
#define MATRIX_COL_MAX  5
#define MATRIX_ROW_MAX  8
#define WHITE_LEDS      6
#define YELLOW_LEDS     8
#define RED_LEDS        8
#define GREEN_LEDS      6
#define BLUE_LEDS       4

// TODO: We need to fix choosing an LED at random since there are holes
#define LED_MAX         60


/*
 * Standardised LED Matrix
 */
const LED leds[LED_MAX] = {
    /*D0         D1          D2          D3          D4          D5          D6          D7          D8          D9*/
    { C0, R0 }, { C0, R1 }, { C0, R2 }, { C0, R3 }, { C0, R4 }, { C0, R5 }, { C0, R6 }, { C0, R7 }, { C0, R8 }, { C0, R9 },
    /*D10        D11         D12         D13         D14         D15         D16         D17         D18         D19*/
    { C1, R0 }, { C1, R1 }, { C1, R2 }, { C1, R3 }, { C1, R4 }, { C1, R5 }, { C1, R6 }, { C1, R7 }, { C1, R8 }, { C1, R9 },
    /*D20        D21         D22         D23         D24         D25         D26         D27         D28         D29*/
    { C2, R0 }, { C2, R1 }, { C2, R2 }, { C2, R3 }, { C2, R4 }, { C2, R5 }, { C2, R6 }, { C2, R7 }, { C2, R8 }, { C2, R9 },
    /*D30        D31         D32         D33         D34         D35         D36         D37         D38         D39*/
    { C3, R0 }, { C3, R1 }, { C3, R2 }, { C3, R3 }, { C3, R4 }, { C3, R5 }, { C3, R6 }, { C3, R7 }, { C3, R8 }, { C3, R9 },
    /*D40        D41         D42         D43         D44         D45         D46         D47         D48         D49*/
    { C4, R0 }, { C4, R1 }, { C4, R2 }, { C4, R3 }, { C4, R4 }, { C4, R5 }, { C4, R6 }, { C4, R7 }, { C4, R8 }, { C4, R9 },
    /*D50        D51         D52         D53         D54         D55         D56         D57         D58         D59*/
    { C5, R0 }, { C5, R1 }, { C5, R2 }, { C5, R3 }, { C5, R4 }, { C5, R5 }, { C5, R6 }, { C5, R7 }, { C5, R8 }, { C5, R9 }
};


/*
 * Pin Maps 
 * Pin to PWM WO
 * PB0 -> WO0 -> C2
 * PB1 -> WO1 -> C3
 * PB2 -> WO2 -> 
 * PA3 -> WO3 -> C1
 * PA4 -> WO4 -> C0
 * PA5 -> WO5 -> C4
 */
#define PIN_WO0  PIN_PB0
#define PIN_WO1  PIN_PB1
#define PIN_WO3  PIN_PA3
#define PIN_WO4  PIN_PA4
#define PIN_WO5  PIN_PA5
#define PIN_R0   PIN_PC3
#define PIN_R1   PIN_PC2
#define PIN_R2   PIN_PC1
#define PIN_R3   PIN_PC0
#define PIN_R4   PIN_PA6
#define PIN_R5   PIN_PA7
#define PIN_R6   PIN_PB5
#define PIN_R7   PIN_PB4


/*
 * LED Matrix Mappings
 */
#define writeMatrixC0(v) analogWriteWO4(v)
#define writeMatrixC1(v) analogWriteWO3(v)
#define writeMatrixC2(v) analogWriteWO0(v)
#define writeMatrixC3(v) analogWriteWO1(v)
#define writeMatrixC4(v) analogWriteWO5(v)
#define writeMatrixR0(v) digitalWriteFast(PIN_R0, v)
#define writeMatrixR1(v) digitalWriteFast(PIN_R1, v)
#define writeMatrixR2(v) digitalWriteFast(PIN_R2, v)
#define writeMatrixR3(v) digitalWriteFast(PIN_R3, v)
#define writeMatrixR4(v) digitalWriteFast(PIN_R4, v)
#define writeMatrixR5(v) digitalWriteFast(PIN_R5, v)
#define writeMatrixR6(v) digitalWriteFast(PIN_R6, v)
#define writeMatrixR7(v) digitalWriteFast(PIN_R7, v)


/*
 * Other pins
 */
#define UNUSED_1 PIN_PA1
#define UNUSED_2 PIN_PA2
#define UNUSED_3 PIN_PB3
#define UNUSED_4 PIN_PB2

/*
 * Use unused pins for random seeding
 */
#define SEED_UNUSED_1
#define SEED_UNUSED_2

/*
 * LED Programs
 */
#include "elephant/elephant_programs.h"


#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
