/*
 * LED Core Common Shared Definitions
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef VARIANT_CORE_COMMON_H
#define VARIANT_CORE_COMMON_H


/*
 * Core settings
 */
#define RANDOM_WAIT_MIN 1
#define RANDOM_WAIT_MAX 10
#define RANDOM_SLEEP_MIN 5
#define RANDOM_SLEEP_MAX 15
#define BRIGHTNESS_FACTOR_DEFAULT 16
#define BRIGHTNESS_FACTOR_MIN 4
#define BRIGHTNESS_FACTOR_STEP 4
#define BRIGHTNESS_FACTOR_MAX 32
#define INITIAL_BRIGHTNESS DIM
#define INITIAL_PERIOD 10
#define FLASH_DURING_SLEEP
#define ENABLE_POST_ALL_CHASE


/*
 * Basic variant metadata
 */
#define MATRIX_COL_MAX  6
#define MATRIX_ROW_MAX  10
#define LED_MAX         60


/*
 * LED matrix layout
 *      | C0  | C1  | C2  | C3  | C4  | C5  |
 * +----+-----+-----+-----+-----+-----+-----+
 * | R0 | D0  | D10 | D20 | D30 | D40 | D50 |
 * | R1 | D1  | D11 | D21 | D31 | D41 | D51 |
 * | R2 | D2  | D12 | D22 | D32 | D42 | D52 |
 * | R3 | D3  | D13 | D23 | D33 | D43 | D53 |
 * | R4 | D4  | D14 | D24 | D34 | D44 | D54 |
 * | R5 | D5  | D15 | D25 | D35 | D45 | D55 |
 * | R6 | D6  | D16 | D26 | D36 | D46 | D56 |
 * | R7 | D7  | D17 | D27 | D37 | D47 | D57 |
 * | R8 | D8  | D18 | D28 | D38 | D48 | D58 |
 * | R9 | D9  | D19 | D29 | D39 | D49 | D59 |
 * 
 */
const LED leds[LED_MAX] = {
    /*D0         D1          D2          D3          D4          D5          D6          D7          D8          D9*/
    { C0, R0 }, { C0, R1 }, { C0, R2 }, { C0, R3 }, { C0, R4 }, { C0, R5 }, { C0, R6 }, { C0, R7 }, { C0, R8 }, { C0, R9 },
    /*D10        D11         D12         D13         D14         D15         D16         D17         D18         D19*/
    { C1, R0 }, { C1, R1 }, { C1, R2 }, { C1, R3 }, { C1, R4 }, { C1, R5 }, { C1, R6 }, { C1, R7 }, { C1, R8 }, { C1, R9 },
    /*D20        D21         D22         D23         D24         D25         D26         D27         D28         D29*/
    { C2, R0 }, { C2, R1 }, { C2, R2 }, { C2, R3 }, { C2, R4 }, { C2, R5 }, { C2, R6 }, { C2, R7 }, { C2, R8 }, { C2, R9 },
    /*D30        D31         D32         D33         D34         D35         D36         D37         D38         D39*/
    { C3, R0 }, { C3, R1 }, { C3, R2 }, { C3, R3 }, { C3, R4 }, { C3, R5 }, { C3, R6 }, { C3, R7 }, { C3, R8 }, { C3, R9 },
    /*D40        D41         D42         D43         D44         D45         D46         D47         D48         D49*/
    { C4, R0 }, { C4, R1 }, { C4, R2 }, { C4, R3 }, { C4, R4 }, { C4, R5 }, { C4, R6 }, { C4, R7 }, { C4, R8 }, { C4, R9 },
    /*D50        D51         D52         D53         D54         D55         D56         D57         D58         D59*/
    { C5, R0 }, { C5, R1 }, { C5, R2 }, { C5, R3 }, { C5, R4 }, { C5, R5 }, { C5, R6 }, { C5, R7 }, { C5, R8 }, { C5, R9 }
};


/*
 * Pin Maps 
 */

#ifndef CORE_CUSTOM_PINOUT
#ifdef CORE_ALT_PINOUT

/* Alternative core moule pinouts */
#define PIN_WO0 PIN_PB0
#define PIN_WO1 PIN_PB1
#define PIN_WO2 PIN_PB2
#define PIN_WO3 PIN_PA3
#define PIN_WO4 PIN_PA4
#define PIN_WO5 PIN_PA5
#define PIN_R0  PIN_PA2
#define PIN_R1  PIN_PA6
#define PIN_R2  PIN_PA7
#define PIN_R3  PIN_PB5
#define PIN_R4  PIN_PB4
#define PIN_R5  PIN_PC3
#define PIN_R6  PIN_PC2
#define PIN_R7  PIN_PC1
#define PIN_R8  PIN_PC0
#define PIN_R9  PIN_PB3
#define PIN_S1  PIN_PA1

#else

/* Default core module pinouts */
#define PIN_WO0 PIN_PB0
#define PIN_WO1 PIN_PB1
#define PIN_WO2 PIN_PB2
#define PIN_WO3 PIN_PA3
#define PIN_WO4 PIN_PA4
#define PIN_WO5 PIN_PA5
#define PIN_R0  PIN_PA2
#define PIN_R1  PIN_PA6
#define PIN_R2  PIN_PA7
#define PIN_R3  PIN_PB5
#define PIN_R4  PIN_PB4
#define PIN_R5  PIN_PB3
#define PIN_R6  PIN_PC0
#define PIN_R7  PIN_PC1
#define PIN_R8  PIN_PC2
#define PIN_R9  PIN_PC3
#define PIN_S1  PIN_PA1

#endif
#endif


/*
 * Use S1 button for random seed data
 */
#define SEED_S1

/*
 * LED Matrix Mappings
 */
#define writeMatrixC0(v) analogWriteWO3(v)
#define writeMatrixC1(v) analogWriteWO4(v)
#define writeMatrixC2(v) analogWriteWO5(v)
#define writeMatrixC3(v) analogWriteWO0(v)
#define writeMatrixC4(v) analogWriteWO1(v)
#define writeMatrixC5(v) analogWriteWO2(v)
#define writeMatrixR0(v) digitalWriteFast(PIN_R0, v)
#define writeMatrixR1(v) digitalWriteFast(PIN_R1, v)
#define writeMatrixR2(v) digitalWriteFast(PIN_R2, v)
#define writeMatrixR3(v) digitalWriteFast(PIN_R3, v)
#define writeMatrixR4(v) digitalWriteFast(PIN_R4, v)
#define writeMatrixR5(v) digitalWriteFast(PIN_R5, v)
#define writeMatrixR6(v) digitalWriteFast(PIN_R6, v)
#define writeMatrixR7(v) digitalWriteFast(PIN_R7, v)
#define writeMatrixR8(v) digitalWriteFast(PIN_R8, v)
#define writeMatrixR9(v) digitalWriteFast(PIN_R9, v)

/*
 * Button interrupt
 */
#define S1_VECTOR      PORTA_PORT_vect
#define S1_PINCTL      PORTA.PIN1CTRL
#define S1_FLAGS       PORTA.INTFLAGS

#ifndef CORE_CUSTOM_BRIGHTNESS_DISPLAY

#define displayBrightnessOn(value) writeMatrixC0(value);\
                                   writeMatrixC3(value);\
                                   writeMatrixC4(value);\
                                   writeMatrixR0(LOW);

#define displayBrightnessOff()     writeMatrixC0(PWM_OFF);\
                                   writeMatrixC3(PWM_OFF);\
                                   writeMatrixC4(PWM_OFF);\
                                   writeMatrixR0(HIGH);

#endif


#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
