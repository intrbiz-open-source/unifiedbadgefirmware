/*
 * Common LED Programs shared across all designs
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef VARIANT_COMMON_PROGRAMS_H
#define VARIANT_COMMON_PROGRAMS_H

START_PROGRAM(PROG_ALL_ON)
  CALL_PATTERN(PATTERN_ALL)
END_PROGRAM

START_PROGRAM(PROG_FLASH_RANDOM)
  START_LOOP(LOOP_A, 100)
  CALL_PATTERN(PATTERN_FLASH_RANDOM)
  END_LOOP(LOOP_A)
END_PROGRAM

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
