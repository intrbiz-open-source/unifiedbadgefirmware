/*
 * Cat design variants
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H

/*
 * Core settings
 */
#define RANDOM_WAIT_MIN 1
#define RANDOM_WAIT_MAX 10
#define RANDOM_SLEEP_MIN 5
#define RANDOM_SLEEP_MAX 15
#define ITER_RANDOM_MIN 1
#define ITER_RANDOM_MAX 30
#define BRIGHTNESS_FACTOR_DEFAULT 16
#define BRIGHTNESS_FACTOR_MIN 4
#define BRIGHTNESS_FACTOR_STEP 4
#define BRIGHTNESS_FACTOR_MAX 32
#define INITIAL_BRIGHTNESS BRIGHT
#define INITIAL_PERIOD 10
#define FLASH_DURING_SLEEP


/*
 * Basic variant metadata
 */
#define MATRIX_COL_MAX  3
#define MATRIX_ROW_MAX  7
#define LED_MAX         20
#define WHITE_LEDS      4
#define YELLOW_LEDS     4
#define RED_LEDS        5
#define GREEN_LEDS      2
#define BLUE_LEDS       5


/*
 * LED matrix layout
 *      | C0  | C1  | C2  |
 * +----+-----+-----+-----+
 * | R0 | D6  | D7  | D8  |
 * | R1 | D9  | D10 | D11 |
 * | R2 | D12 | D13 | D14 |
 * | R3 | D15 | D16 | D17 |
 * | R4 | D18 | D19 |     |
 * | R5 | D1  | D2  | D0  |
 * | R6 | D3  | D5  | D4  |
 * 
 */
const LED leds[LED_MAX] = {
    /*D0         D1          D2          D3          D4          D5          D6          D7          D8          D9*/
    { C2, R5 }, { C0, R5 }, { C1, R5 }, { C0, R6 }, { C2, R6 }, { C1, R6 }, { C0, R0 }, { C1, R0 }, { C2, R0 }, { C0, R1 },
    /*D10        D11         D12         D13         D14         D15         D16         D17         D18         D19*/
    { C1, R2 }, { C2, R1 }, { C0, R2 }, { C1, R2 }, { C2, R2 }, { C0, R3 }, { C1, R3 }, { C2, R3 }, { C0, R4 }, { C1, R4 }
};


/*
 * Pin Maps 
 */
#define PIN_WO0  PIN_PB0
#define PIN_WO1  PIN_PB1
#define PIN_WO2  PIN_PB2
#define PIN_R0   PIN_PC0
#define PIN_R1   PIN_PC1
#define PIN_R2   PIN_PC2
#define PIN_R3   PIN_PC3
#define PIN_R4   PIN_PA4
#define PIN_R5   PIN_PB4
#define PIN_R6   PIN_PB3


/*
 * LED Matrix Mappings
 */
#define writeMatrixC0(v) analogWriteWO1(v)
#define writeMatrixC1(v) analogWriteWO2(v)
#define writeMatrixC2(v) analogWriteWO0(v)
#define writeMatrixR0(v) digitalWriteFast(PIN_R0, v)
#define writeMatrixR1(v) digitalWriteFast(PIN_R1, v)
#define writeMatrixR2(v) digitalWriteFast(PIN_R2, v)
#define writeMatrixR3(v) digitalWriteFast(PIN_R3, v)
#define writeMatrixR4(v) digitalWriteFast(PIN_R4, v)
#define writeMatrixR5(v) digitalWriteFast(PIN_R5, v)
#define writeMatrixR6(v) digitalWriteFast(PIN_R6, v)


/*
 * Other pins
 */
#define SENSOR_LIGHT    PIN_PA7
#define UNUSED_INPUT_1  PIN_PB5
#define UNUSED_INPUT_2  PIN_PA1
#define UNUSED_1        PIN_PA5
#define UNUSED_2        PIN_PA3
#define UNUSED_3        PIN_PA2
#define UNUSED_4        PIN_PA6

/*
 * Use unused pins for random seeding
 */
#define SEED_UNUSED_1
#define SEED_UNUSED_2


/*
 * Nice Matrix Column And Row Names
 */
#define COL_BLUE     C0
#define COL_RED      C1
#define COL_YELLOW   C2
#define COL_WHITE_1  C0
#define COL_WHITE_2  C1
#define COL_WHITE_3  C2
#define COL_GREEN_1  C0
#define COL_GREEN_2  C2
#define ROW_BLUE_1   R0
#define ROW_BLUE_2   R1
#define ROW_BLUE_3   R2
#define ROW_BLUE_4   R3
#define ROW_BLUE_5   R4
#define ROW_RED_1    R0
#define ROW_RED_2    R1
#define ROW_RED_3    R2
#define ROW_RED_4    R3
#define ROW_RED_5    R4
#define ROW_YELLOW_1 R0
#define ROW_YELLOW_2 R1
#define ROW_YELLOW_3 R2
#define ROW_YELLOW_4 R3
#define ROW_WHITE_1  R5
#define ROW_WHITE_2  R6
#define ROW_GREEN_1  R5


/*
 * Nice LED Names
 */
#define RIGHT_EYE D0
#define LEFT_EYE  D1
#define GREEN_1   D0
#define GREEN_2   D1
#define WHITE_1   D2
#define WHITE_2   D3
#define WHITE_3   D4
#define WHITE_4   D5
#define BLUE_1    D6
#define BLUE_2    D9
#define BLUE_3    D12
#define BLUE_4    D15
#define BLUE_5    D18
#define YELLOW_1  D8
#define YELLOW_2  D11
#define YELLOW_3  D14
#define YELLOW_4  D17
#define RED_1     D7
#define RED_2     D10
#define RED_3     D13
#define RED_4     D16
#define RED_5     D19


/*
 * Subprograms
 */
#include "cat/subprograms.h"
START_SUBPROGRAMS
  SUBPROGRAM(0,  SUB_CYCLE_ALL_LEDS)
  SUBPROGRAM(1,  SUB_CHASE_RED_UP)
  SUBPROGRAM(2,  SUB_CHASE_RED_DOWN)
  SUBPROGRAM(3,  SUB_CHASE_BLUE_UP)
  SUBPROGRAM(4,  SUB_CHASE_BLUE_DOWN)
  SUBPROGRAM(5,  SUB_CHASE_YELLOW_UP)
  SUBPROGRAM(6,  SUB_CHASE_YELLOW_DOWN)
  SUBPROGRAM(7,  SUB_CHASE_WHITE_UP)
  SUBPROGRAM(8,  SUB_CHASE_WHITE_DOWN)
  SUBPROGRAM(9,  SUB_FLASH_EYES)
  SUBPROGRAM(10, SUB_TWINKLE_EYES)
  SUBPROGRAM(11, SUB_CHASE_FLAG_UP)
  SUBPROGRAM(12, SUB_CHASE_FLAG_DOWN)
  SUBPROGRAM(13, SUB_CHASE_BLUE_RED_UP)
  SUBPROGRAM(14, SUB_CHASE_BLUE_RED_DOWN)
END_SUBPROGRAMS


/*
 * Programs
 */
#include "cat/programs.h"
START_PROGRAMS
  PROGRAM(0,  PROG_CYCLE_MULTILOOP_LEDS)
  PROGRAM(1,  PROG_CYCLE_RANDOM_PATTERN)
  PROGRAM(2,  PROG_CYCLE_RANDOM_LED)
  PROGRAM(3,  PROG_CYCLE_LEDS)
  PROGRAM(4,  PROG_EYES)
  PROGRAM(5,  PROG_FLASH_ALL)
  PROGRAM(6,  PROG_CHASE_BARS)
  PROGRAM(7,  PROG_WHITE_BAR_BOUNCE)
  PROGRAM(8,  PROG_FAST_BARS_ZIGZAG)
  PROGRAM(9,  PROG_STROKE_UP_DOWN)
  PROGRAM(10, PROG_FLAG_STAIRS)
  PROGRAM(11, PROG_FLAG_JUMP)
  PROGRAM(12, PROG_WHITE_OUT)
  PROGRAM(13, PROG_FLAG_SPIRAL)
  PROGRAM(14, PROG_FLAG_IMPLODE)
  PROGRAM(15, PROG_FLAG_EXPLODE)
  PROGRAM(16, PROG_WHITE_SPEEDUP)
  PROGRAM(17, PROG_RED_BLUE_SPEEDUP)
  PROGRAM(18, PROG_YELLOW_SPEEDUP)
  PROGRAM(19, PROG_YELLOW_SLOWDOWN)
  PROGRAM(20, PROG_WHITE_YELLOW_SPEEDUP)
  PROGRAM(21, PROG_YELLOW_WHITE_SLOWDOWN)
  PROGRAM(22, PROG_RED_BLUE_SLOWDOWN)
  PROGRAM(23, PROG_WHITE_RANDOM_FAST)
  PROGRAM(24, PROG_RED_RANDOM_FAST)
  PROGRAM(25, PROG_BLUE_RANDOM_FAST)
  PROGRAM(26, PROG_YELLOW_RANDOM_FAST)
  PROGRAM(27, PROG_RED_BLUE_RANDOM_FAST)
  PROGRAM(28, PROG_FLAG_RANDOM_FAST)
  PROGRAM(29, PROG_FLAG_EYES_RANDOM_FAST)
  PROGRAM(30, PROG_FLAG_RANDOM_VARISPEED)
  PROGRAM(31, PROG_CYCLE_RANDOM_LED_RANDOM_SPEED)
  PROGRAM(32, PROG_CYCLE_RANDOM_PATTERN_RANDOM_SPEED)
END_PROGRAMS


#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
