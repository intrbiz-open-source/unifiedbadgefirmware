/*
 * Elephant design variants
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H


/* Include the common core module with alternative pinouts */
#define CORE_ALT_PINOUT
#define CORE_CUSTOM_BRIGHTNESS_DISPLAY
#include "common/core.h"

/*
 * Customised brightness display routine
 */
#define displayBrightnessOn(value)  writeMatrixC5(value);\
                                    writeMatrixC4(value);\
                                    writeMatrixR5(LOW);\
                                    writeMatrixR6(LOW);\
                                    writeMatrixR7(LOW);\
                                    writeMatrixR8(LOW);

#define displayBrightnessOff()      writeMatrixC5(PWM_OFF);\
                                    writeMatrixC4(PWM_OFF);\
                                    writeMatrixR5(HIGH);\
                                    writeMatrixR6(HIGH);\
                                    writeMatrixR7(HIGH);\
                                    writeMatrixR8(HIGH);

/*
 * LED Programs
 */
#include "elephant_v2/elephantv2_programs.h"


#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
