/*
 * LED Core Test
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H


/* Include the common core module pinouts */
#include "common/core.h"

/* Scan all LEDs on power on */
#define ENABLE_POST

/*
 * Subprograms
 */
#include "test/subprograms.h"
START_SUBPROGRAMS
  SUBPROGRAM(0,  SUB_CYCLE_ALL_LEDS)
END_SUBPROGRAMS


/*
 * Programs
 */
#include "test/programs.h"
START_PROGRAMS
  PROGRAM(0,  PROG_CYCLE_ALL_LEDS)
  PROGRAM(1,  PROG_CALL_SUB_CYCLE_ALL_LEDS)
END_PROGRAMS


#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
