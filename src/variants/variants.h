/*
 * Include the variant definition
 * 
 * Copyright - Chris Ellis - 2023
 */

#ifndef VARIANTS_H
#define VARIANTS_H

#include "../../config.h"

/*
 * Variant definitions
 */

/* LED Core Test */
#ifdef IM_A_CORE_TEST
#include "common.h"
#include "core_test.h"
#endif

/* Cat badge */
#ifdef IM_A_CAT
#include "common.h"
#include "cat.h"
#endif

/* Elephant badge */
#ifdef IM_A_ELEPHANT
#include "common.h"
#include "elephant.h"
#endif

/* Puffin badge */
#ifdef IM_A_PUFFIN
#include "common.h"
#include "puffin.h"
#endif

/* Toucan badge */
#ifdef IM_A_TOUCAN
#include "common.h"
#include "toucan.h"
#endif

/* PGDay UK Elephant badge */
#ifdef IM_A_PGDAYUK_ELEPHANT
#include "common.h"
#include "pgdayuk_elephant.h"
#endif

/* Elephant V2 badge */
#ifdef IM_A_ELEPHANT_V2
#include "common.h"
#include "elephant_v2.h"
#endif

/* Elephant V3 badge */
#ifdef IM_A_ELEPHANT_V3
#include "common.h"
#include "elephantv3.h"
#endif

/* Elephant NL badge */
#ifdef IM_A_ELEPHANT_NL
#include "common.h"
#include "elephantnl.h"
#endif

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
