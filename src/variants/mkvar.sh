#!/bin/sh

NAME=$1

mkdir $NAME

cat > ./${NAME}.h << EOF
/*
 * ${NAME} LED Badge
 * 
 * Copyright - Chris Ellis - 2024
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H

/* Include the common core module */
#include "common/core.h"

/*
 * LED Programs
 */
#include "${NAME}/${NAME}_programs.h"

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
EOF
