/*
 * elephantnl LED Badge
 * 
 * Copyright - Chris Ellis - 2024
 */
#ifndef THE_VARIANT_H
#define THE_VARIANT_H

/* Include the common core module */
#define CORE_ALT_PINOUT
#include "common/core.h"

/*
 * LED Programs
 */
#include "elephantnl/elephantnl_programs.h"

#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
