/*
 * LED Badge firmware
 * 
 * Copyright - Chris Ellis - 2023
 */

/*
 * See options.h to set the build variant
 */

/*
 * The LED runtime
 */
#include "src/engine/runtime.h"


void setup()
{
  setupRuntime();
}

void loop()
{
  loopRuntime();
}
