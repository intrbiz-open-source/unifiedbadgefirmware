/*
 * Build options
 * 
 * Copyright - Chris Ellis - 2023
 */
#ifndef OPTIONS_H
#define OPTIONS_H


/*
 * Select the variant
 */
//##define IM_A_CORE_TEST

//#define IM_A_CAT
//#define IM_A_ELEPHANT
//#define IM_A_PUFFIN
//#define IM_A_TOUCAN
//#define IM_A_ELEPHANT_V2
//#define IM_A_ELEPHANT_V3
#define IM_A_ELEPHANT_NL
//#define IM_A_PGDAYUK_ELEPHANT



#endif
// kate: space-indent on; indent-width 2; mixedindent off; indent-mode cstyle;
