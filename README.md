# Unified Badge Firmware

## About
This is the firmware behind the LED badges (https://intrbiz.com/electronics/led-pcb-art/) that I've made.

This is designed to run on a ATTiny1616 and control upto 32 LEDs wired in a matrix.  The firmware constains 
a very simple OP Code interpreter, allowing LED flash patterns to be defined as an array to bytes.  This 
enables for very space efficient storage of LED flash patterns and efficient execution.

The OP Codes are defined in: [ops.h](src/engine/ops.h) and programs can be defined using a DSL implemented 
usin C Macros.

Eg: [subprograms.h](src/variant/elephant/subprograms.h) [programs.h](src/variant/elephant/programs.h)

## Building

The firmware can be built in the Arduino IDE, with the [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) 
core installed.  The firmware can be uploaded using a simple UDPI programmer made from a diode and a USB TTL 
Serial adapter.

Edit `config.h` to set the Badge variant you wish to build the firmware for by selecting one `IM_A_...` define.

### Build Settings

The Arduino core should be configured with the following options:

![Arduino Build Settings](build_settings.png)

## License

Unified Badge Firmware
Copyright (c) 2023, Chris Ellis
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Author

Chris Ellis

Mastodon: @intrbiz@bergamot.social

Web: https://intrbiz.com/

Copyright (c) Chris Ellis 2023
